package net.consentmanager.consentmangertvexample;

import android.os.Bundle;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import net.consentmanager.tvsdk.CMPConsentTool;
import net.consentmanager.tvsdk.core.callbacks.OnCloseCallback;
import net.consentmanager.tvsdk.core.callbacks.OnOpenCallback;
import net.consentmanager.tvsdk.storage.CMPStorageV1;
import net.consentmanager.tvsdk.storage.CMPStorageV2;

/*
 * Main Activity class that loads {@link MainFragment}.
 */
public class MainActivity extends FragmentActivity {
    private TextView gdprInfoTextView;
    private CMPConsentTool consentTool;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CMPConsentTool.clearAllValues(this);
        CMPConsentTool.setLogMode(true);
        int id = 123456;                                // ID of the CMP, e.g. 123
        String server_domain = "www.consentmanager.net"; // Server domain, e.g. “consentmanager.net”
        String app_name = "test";            // App name or other identifier, e.g. „My Example App“
        String language = "DE";

        consentTool = CMPConsentTool.createInstance(this.getApplicationContext(), id, server_domain, app_name, language, 5000, new OnOpenCallback() {
            public void onWebViewOpened() {
                System.out.println("------------------------------------------------\nWebViewOpened\n----------------------------------------------------");
            }
        }, new OnCloseCallback() {
            public void onWebViewClosed() {
                System.out.println("------------------------------------------------\nWebViewClosed\n----------------------------------------------------");
                System.out.println("TCString:" + CMPStorageV2.getTCString(MainActivity.this.getApplicationContext()));
                for( int i = 1; i <= 20; i++){
                    for( int s = 400; s <= 810; s++){
                        if( consentTool.hasPurposeConsentForVendor(MainActivity.this, i, s)){
                            System.out.println(String.format("Purpose %d Vendor %d is set!", i, s));
                        }
                    }
                }


                System.out.println(String.format("TCString Erhalten: %s", CMPStorageV2.getTCString(MainActivity.this)));
                System.out.println(String.format("PublisherCustomPurposesLegitimateInterests Erhalten: %s", CMPStorageV2.getPublisherCustomPurposesLegitimateInterests(MainActivity.this)));
                System.out.println(String.format("PublisherCustomPurposesConsents Erhalten: %s", CMPStorageV2.getPublisherCustomPurposesConsents(MainActivity.this)));
                System.out.println(String.format("PublisherLegitimateInterests Erhalten: %s", CMPStorageV2.getPublisherLegitimateInterests(MainActivity.this)));
                System.out.println(String.format("PublisherConsent Erhalten: %s", CMPStorageV2.getPublisherConsent(MainActivity.this)));
                System.out.println(String.format("SpecialFeaturesOptIns Erhalten: %s", CMPStorageV2.getSpecialFeaturesOptIns(MainActivity.this)));
                System.out.println(String.format("PurposeLegitimateInterests Erhalten: %s", CMPStorageV2.getPurposeLegitimateInterests(MainActivity.this)));
                System.out.println(String.format("PurposeConsents Erhalten: %s", CMPStorageV2.getPurposeConsents(MainActivity.this)));
                System.out.println(String.format("VendorLegitimateInterests Erhalten: %s", CMPStorageV2.getVendorLegitimateInterests(MainActivity.this)));
                System.out.println(String.format("VendorConsents Erhalten: %s", CMPStorageV2.getVendorConsents(MainActivity.this)));
                System.out.println(String.format("UseNonStandardStacks Erhalten: %s", CMPStorageV2.getUseNonStandardStacks(MainActivity.this)));
                System.out.println(String.format("PurposeOneTreatment Erhalten: %s", CMPStorageV2.getPurposeOneTreatment(MainActivity.this)));
                System.out.println(String.format("PublisherCC: %s", CMPStorageV2.getPublisherCC(MainActivity.this)));
                System.out.println(String.format("GDPRApplies: %s", CMPStorageV2.getGDPRApplies(MainActivity.this)));
                System.out.println(String.format("PolicyVersion Erhalten: %s", CMPStorageV2.getPolicyVersion(MainActivity.this)));
                System.out.println(String.format("SDKVersion Erhalten: %s", CMPStorageV2.getSDKVersion(MainActivity.this)));
                System.out.println(String.format("SDKID Erhalten: %s", CMPStorageV2.getSDKID(MainActivity.this)));


                for( int i = 1; i <= 10; i++){
                    System.out.println(consentTool.hasPurposeConsent(MainActivity.this, i+"", true));
                }

                for( int i = 1; i <= 10; i++){
                    System.out.println(consentTool.hasVendorConsent(MainActivity.this, i+"", true));
                }
            }
        }); //All Parameters

        // Style View Buttons
        //Set The button, that should open the CMPConsentToolView, if it is clicked
        //consentTool.setOpenCmpConsentToolViewListener(gdprButton);

        //Set The button, that should open the CMPConsentToolView, if it is clicked and the
        //listener that should be called, if the CMPConsentToolView is submitted by the user.

        //Reaction to a Network Error:
        /*consentTool.setNetworkExceptionListener(new OnNetworkExceptionCallback() {
            @Override
            public void onErrorOccur(String message) {
                System.out.println(message);
            }
        });*/

        //Create Listener for State Button
    }

    private String getGdprInfo() {
        return String.format("vendors: %s\npurposes: %s\nsubjectToGDPR: %s\n consentString: %s",
                consentTool.getVendorsString(this),
                consentTool.getPurposes(this) + " ----- " +
                        CMPStorageV1.getPurposesString(this) + " ------- " +
                        CMPStorageV2.getPurposeConsents(this),
                CMPStorageV2.getGDPRApplies(this),
                CMPStorageV2.getTCString(this));
    }
}