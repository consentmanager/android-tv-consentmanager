package net.consentmanager.tvsdk.server;

import net.consentmanager.tvsdk.model.CMPConfig;

class CmpStatusUrl {
    String url;

    public CmpStatusUrl(CMPConfig config, String consent, String idfa){

        this.url = String.format("https://%s/delivery/appjson.php?id=%s&name=%s&consent=%s&idfa=%s&l=%s",
                config.getServerDomain(),
                config.getId(),
                config.getAppName(),
                consent,
                idfa,
                config.getLanguage());
    }

    public String get(){
        return url;
    }
}
