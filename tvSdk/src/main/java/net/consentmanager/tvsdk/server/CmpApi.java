package net.consentmanager.tvsdk.server;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;

import net.consentmanager.tvsdk.CMPConsentTool;
import net.consentmanager.tvsdk.core.exceptions.CMPConsentToolNetworkException;
import net.consentmanager.tvsdk.model.CMPConfig;
import net.consentmanager.tvsdk.model.CMPSettings;
import net.consentmanager.tvsdk.model.SubjectToGdpr;
import net.consentmanager.tvsdk.storage.CMPPrivateStorage;
import net.consentmanager.tvsdk.storage.CMPStorageConsentManager;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;


public class CmpApi {

    public static CmpStatus getAndSaveResponse(CMPConfig config, final Context context, int timeout) throws CMPConsentToolNetworkException {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnected() && activeNetwork.isAvailable();
        if( isConnected) {
            CmpStatus response = proceedRequest(config, context, timeout, "");
            CMPSettings.setConsentToolUrl(context, response.getUrl());
            CMPSettings.setSubjectToGdpr(context, response.getRegulation() == 1 ? SubjectToGdpr.CMPGDPREnabled : SubjectToGdpr.CMPGDPRDisabled);
            return response;
        } else {
            CMPConsentTool.log("Network is not available");
            throw new CMPConsentToolNetworkException("Network is not available");
        }
    }

    public static CmpStatus getAndSaveResponse(CMPConfig config, final Context context, int timeout, String idfa) throws CMPConsentToolNetworkException {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnected() && activeNetwork.isAvailable();
        if( isConnected) {
            CmpStatus response = proceedRequest(config, context, timeout, idfa);
            CMPSettings.setConsentToolUrl(context, response.getUrl());
            CMPSettings.setSubjectToGdpr(context, response.getRegulation() == 1 ? SubjectToGdpr.CMPGDPREnabled : SubjectToGdpr.CMPGDPRDisabled);
            return response;
        } else {
            CMPConsentTool.log("Network is not available");
            throw new CMPConsentToolNetworkException("Network is not available");
        }
    }

    private static CmpStatus proceedRequest(CMPConfig config, final Context context, int timeout, String idfa) throws CMPConsentToolNetworkException {

        String consent = CMPStorageConsentManager.getConsentString(context);
        final String url_string = new CmpStatusUrl(config, consent, idfa).get();
        CMPConsentTool.log("Contacting the consentmanager Server with: " + url_string);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            URL url = new URL(url_string);

            HttpURLConnection huc = (HttpURLConnection) url.openConnection();
            huc.setInstanceFollowRedirects(true);
            huc.setConnectTimeout(timeout);
            huc.setRequestMethod("GET");
            huc.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
            huc.connect();
            CMPConsentTool.log(huc.getResponseMessage());


            InputStream input = huc.getInputStream();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            input));

            String inputLine = in.readLine();
            in.close();
            CMPPrivateStorage.setLastRequested(context, new Date());
            StrictMode.enableDefaults();
            StrictMode.setThreadPolicy(StrictMode.allowThreadDiskReads());
            StrictMode.setThreadPolicy(StrictMode.allowThreadDiskWrites());
            Gson g = new Gson();
            return g.fromJson(inputLine, CmpStatus.class);
        } catch( Exception e){
            StrictMode.enableDefaults();
            throw new CMPConsentToolNetworkException(e.getMessage());
        }

    }
}
