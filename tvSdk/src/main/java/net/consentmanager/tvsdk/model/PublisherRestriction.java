package net.consentmanager.tvsdk.model;

import java.util.LinkedList;
import java.util.List;

public class PublisherRestriction{
    private List<PublisherRestrictionType> restrictionTypes;
    private int purposeId;


    public PublisherRestriction(int pId, PublisherRestrictionType restrictionType){
        purposeId = pId;
        restrictionTypes = new LinkedList<>();
        restrictionTypes.add(restrictionType);
    }

    public PublisherRestriction(int pId, String restrictionTypes){
        purposeId = pId;
        this.restrictionTypes = new LinkedList<>();
        for( int i = 1; i <= 3; i++){
            if( restrictionTypes.contains(i + "")){
                String data = restrictionTypes;
                for( int r = 1; r <= 3; r++){
                    if( r != i){
                        data = data.replace(r + "", "0");
                    }
                }
                PublisherRestrictionType rtv = new PublisherRestrictionType( i , data);
                this.restrictionTypes.add(rtv);
            }
        }
    }

    public int getPurposeId(){
        return purposeId;
    }

    public List<PublisherRestrictionType> getRestrictionTypes(){
        return restrictionTypes;
    }

    public void setPurposeId(int pId){
        purposeId = pId;
    }

    public void addRestrictionType(PublisherRestrictionType rType){
        restrictionTypes.add(rType);
    }

    public String getVendorsAsNSUSerDefaultsString(){
        StringBuilder defStr = new StringBuilder();
        int maxLength = 0;
        for( int i = 0; i < restrictionTypes.size(); i++){
            if( restrictionTypes.get(i).vendorIds.length() > maxLength){
                maxLength = restrictionTypes.get(i).vendorIds.length();
            }
        }

        for( int v = 1; v <= maxLength; v++){
            boolean found = false;
            for( int i = 0; i < restrictionTypes.size(); i++){
                if( restrictionTypes.get(i).hasVendorId(v)){
                    defStr.append(restrictionTypes.get(i).getRestrictionType());
                    found = true;
                    i = restrictionTypes.size();
                }
            }
            if( ! found ){
                defStr.append("0");
            }
        }

        return defStr.toString();
    }

    public boolean hasVendor(int vendorId){
        for( int i = 0; i < restrictionTypes.size(); i++){
            if( restrictionTypes.get(i).hasVendorId(vendorId) ){
                return true;
            }
        }
        return false;
    }
}
