package net.consentmanager.tvsdk.model.range;

public class RangeEntry {

    private char singleOrRange;
    private String singleVendorId;
    private String startVendorId;
    private String endVendorId;

    public char getSingleOrRange() {
        return singleOrRange;
    }

    public void setSingleOrRange(char singleOrRange) {
        this.singleOrRange = singleOrRange;
    }

    public String getSingleVendorId() {
        return singleVendorId;
    }

    public void setSingleVendorId(String singleVendorId) {
        this.singleVendorId = singleVendorId;
    }

    public String getStartVendorId() {
        return startVendorId;
    }

    public void setStartVendorId(String startVendorId) {
        this.startVendorId = startVendorId;
    }

    public String getEndVendorId() {
        return endVendorId;
    }

    public void setEndVendorId(String endVendorId) {
        this.endVendorId = endVendorId;
    }

    @Override
    public String toString() {
        return "\nsingleOrRange=" + singleOrRange +
                "\nsingleVendorId=" + singleVendorId +
                "\nstartVendorId=" + startVendorId +
                "\nendVendorId=" + endVendorId;
    }
}
