package net.consentmanager.tvsdk.model;

public class PublisherRestrictionType {

    int restrictionType;
    String vendorIds;

    public PublisherRestrictionType(int restrictionType, String vendorIds) {
        this.restrictionType = restrictionType;
        this.vendorIds = vendorIds;
    }

    public String getVendorIds(){
        return vendorIds;
    }

    public int getRestrictionType(){
        return restrictionType;
    }

    public void setVendorIds(String vId){
        vendorIds = vId;
    }

    public void setRestrictionType(int rType){
        restrictionType = rType;
    }

    public boolean hasVendorId(int vId){
        if(vendorIds.length() < vId || vId <= 0){
            return false;
        }
        return vendorIds.charAt(vId - 1) != '0';
    }
}
