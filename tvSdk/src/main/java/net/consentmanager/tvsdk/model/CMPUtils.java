package net.consentmanager.tvsdk.model;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.util.Base64;

import net.consentmanager.tvsdk.CMPConsentTool;
import net.consentmanager.tvsdk.model.decoder.ConsentStringDecoderV1;
import net.consentmanager.tvsdk.model.decoder.ConsentStringDecoderV2;
import net.consentmanager.tvsdk.storage.CMPPrivateStorage;
import net.consentmanager.tvsdk.storage.CMPStorageConsentManager;
import net.consentmanager.tvsdk.storage.CMPStorageV1;
import net.consentmanager.tvsdk.storage.CMPStorageV2;

public class CMPUtils {

    public static String cleanBase64ForBinary(String base64) {
        base64 = base64.replaceAll("%2B", "+");
        base64 = base64.replaceAll("%2F", "/");
        base64 = base64.replaceAll("-", "+");
        base64 = base64.replaceAll("_", "/");
        base64 = base64.replaceAll(" ", "+");
        base64 = base64.replaceAll("%2B", "+");
        base64 = base64.replaceAll("=", "");
        CMPConsentTool.log("cleaned Base64:" + base64);
        return base64;
    }

    public static String cleanBase64(String base64){
        base64 = cleanBase64ForBinary(base64);
        int len = base64.length();
        if( len % 4 > 0 && base64.charAt(len -1) != '='){
            base64 = base64 + "=";
        }
        CMPConsentTool.log("added Padding Base64:" + base64);
        return base64;
    }

    public static void parseAndSaveCMPDataString(Context context, String cmpData){
        try {
            CMPConsentTool.log("received the following CMPData String:");
            CMPConsentTool.log(cmpData);

            if (cmpData != null) {
                CMPConsentTool.log("replacing the useless content from the String");
                cmpData = cmpData.replace("consent://", "");
            }

            if (cmpData != null && cmpData.length() > 0 && cmpData != "null" && cmpData != "nil") {
                CMPConsentTool.log("consentString base64 encoded saved to Shared Preferences");
                CMPStorageConsentManager.setConsentString(context, cmpData);
                CMPPrivateStorage.setNeedsAcceptance(context, false);
                //decode of (consentString + consentmanager Data separated with # and encoded base64)

                String fullString = base64Decode(CMPUtils.cleanBase64(cmpData));
                CMPConsentTool.log("decoded the base 64 encoded String with consentManager extension:");
                CMPConsentTool.log(fullString);

                //get the consentManager data
                String[] splits = fullString.split("#");
                CMPConsentTool.log("checking, if the provided decoded String has the right Syntax");

                if (splits.length > 3) {
                    CMPConsentTool.log("Seems, like the String is correct");
                    proceedConsentString(context, splits[0]);
                    proceedConsentManagerValues(context, splits);

                } else {
                    CMPConsentTool.log("Seems, like the String is incorrect, decoded String needs to have the format: <consentString>#<vendors>#<purposes>#<usprivacy>#<googleAC> The parts seperated with # are added by consentmanager");
                    CMPConsentTool.log("Fatal Error, cleaning all values from the shared preferences");
                    CMPStorageV1.clearConsents(context);
                    CMPStorageV2.clearConsents(context);
                    CMPStorageConsentManager.clearConsents(context);
                }

            } else {
                CMPConsentTool.log("Seems, like the String is incorrect, decoded String needs to have the format: <consentString>#<vendors>#<purposes>#<usprivacy>#<googleAC> The parts seperated with # are added by consentmanager");
                CMPConsentTool.log("Fatal Error, cleaning all values from the shared preferences");
                CMPStorageV1.clearConsents(context);
                CMPStorageV2.clearConsents(context);
                CMPStorageConsentManager.clearConsents(context);
            }
        } catch (Exception e){
            CMPConsentTool.log(e.getMessage());
            e.printStackTrace();
            CMPConsentTool.log(String.format("Consent String %s is in a wrong base64.",cmpData));
            CMPConsentTool.log("Fatal Error, cleaning all values from the shared preferences");
        }
    }

    //Extracts and saves the ConsentManager values
    private static void proceedConsentManagerValues(Context context, String[] splits) {
        if( splits.length > 1) CMPStorageConsentManager.setPurposesString(context, splits[1]);
        if( splits.length > 2) CMPStorageConsentManager.setVendorsString(context, splits[2]);
        if( splits.length > 3) CMPStorageConsentManager.setUSPrivacyString(context, splits[3]);
        if( splits.length > 4) CMPStorageConsentManager.setGoogleACString(context, splits[4]);
    }

    public static boolean hasInternetConnection(Context context){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        CMPConsentTool.log(cm.toString());
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected() && activeNetwork.isAvailable();
    }

    //Saves the consentString and Extracts all Information from the consentString, based on the used Method.
    private static void proceedConsentString(Context context, String value) {
        if(value.charAt(0) == 'B'){
            CMPStorageV2.clearConsents(context);
            CMPConsentTool.log("V1 Consent String detected");
            new ConsentStringDecoderV1().processConsentString(context, value);
        } else if(value.charAt(0) == 'C'){
            CMPStorageV1.clearConsents(context);
            CMPConsentTool.log("V2 Consent String detected");
            ConsentStringDecoderV2 csdv2 = new ConsentStringDecoderV2();
            csdv2.processConsentString(context, value);
        }
    }

    public static StrictMode.ThreadPolicy enableDiskWrites(){
        StrictMode.ThreadPolicy oldThreadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(
                new StrictMode.ThreadPolicy.Builder(oldThreadPolicy)
                        .permitDiskWrites().build());
        return oldThreadPolicy;
    }
    public static void resetStrictMode(StrictMode.ThreadPolicy oldThreadPolicy){
        StrictMode.setThreadPolicy(oldThreadPolicy);
    }
    public static boolean resetStrictMode(StrictMode.ThreadPolicy oldThreadPolicy, boolean returnval){
        StrictMode.setThreadPolicy(oldThreadPolicy);
        return returnval;
    }
    public static String resetStrictMode(StrictMode.ThreadPolicy oldThreadPolicy, String returnval){
        StrictMode.setThreadPolicy(oldThreadPolicy);
        return returnval;
    }
    public static Integer resetStrictMode(StrictMode.ThreadPolicy oldThreadPolicy, Integer returnval){
        StrictMode.setThreadPolicy(oldThreadPolicy);
        return returnval;
    }
    public static StrictMode.ThreadPolicy enableDiskReads(){
        StrictMode.ThreadPolicy oldThreadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(
                new StrictMode.ThreadPolicy.Builder(oldThreadPolicy)
                        .permitDiskReads().build());
        return oldThreadPolicy;
    }
    public static StrictMode.ThreadPolicy enableDiskReadsWrites(){
        StrictMode.ThreadPolicy oldThreadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(
                new StrictMode.ThreadPolicy.Builder(oldThreadPolicy)
                        .permitDiskReads().permitDiskWrites().build());
        return oldThreadPolicy;
    }

    public static Integer binaryToDecimal(String buffer, int startIndex, int endIndex) {
        return binaryToDecimalLength(buffer, startIndex, (endIndex - startIndex));
    }

    public static Integer binaryToDecimalLength(String buffer, int startIndex, int totalOffset) {
        if( buffer == null)
            return 0;
        int length =  buffer.length();

        if (length <= startIndex + totalOffset - 1) {
            return 0;
        }

        int total = 0;
        int from = (startIndex + totalOffset - 1);
        for (int i=from; i >=startIndex; i--) {
            if (buffer.charAt(i) == '1') {
                total +=Math.pow(2, Math.abs(from - i));
            }
        }

        return total;
    }

    private static byte[] getBase64DecodedByteArray(String value){
        byte[] dst = new byte[0];
        try {
            dst = Base64.decode(value, Base64.DEFAULT);
        } catch(Exception e) {
            try {
                dst = Base64.decode(value, Base64.URL_SAFE);
            } catch(Exception e1) {
                try {
                    dst = Base64.decode(value, Base64.NO_PADDING);
                } catch(Exception e2) {
                    try {
                        dst = Base64.decode(value, Base64.CRLF);
                    } catch(Exception e3) {
                        try {
                            dst = Base64.decode(value, Base64.NO_WRAP);
                        } catch(Exception e4) {
                            try {
                                dst = Base64.decode(value, Base64.NO_CLOSE);
                            } catch(Exception e10) {
                                if (value.endsWith("=")) {
                                    value = value.substring(0, value.length() - 1);
                                    try {
                                        dst = Base64.decode(value, Base64.DEFAULT);
                                    } catch (Exception e5) {
                                        try {
                                            dst = Base64.decode(value, Base64.URL_SAFE);
                                        } catch (Exception e6) {
                                            try {
                                                dst = Base64.decode(value, Base64.NO_PADDING);
                                            } catch (Exception e7) {
                                                try {
                                                    dst = Base64.decode(value, Base64.CRLF);
                                                } catch (Exception e8) {
                                                    try {
                                                        dst = Base64.decode(value, Base64.NO_WRAP);
                                                    } catch (Exception e9) {
                                                        try {
                                                            dst = Base64.decode(value, Base64.NO_CLOSE);
                                                        }catch (Exception e11) {
                                                            CMPConsentTool.log("Wrong String for base64 Decoder");
                                                            dst = new byte[0];
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return dst;
    }

    public static String binaryConsentFrom(String split) {

        byte[] dst = getBase64DecodedByteArray(split);
        StringBuilder sb = new StringBuilder();
        for( byte b : dst){
            sb.append( getBinaryByte(b));
        }
        CMPConsentTool.log("Binary:" + sb.toString());
        return sb.toString();
    }

    private static String getBinaryByte(byte b) {
        int val = b;
        if( b < 0){
            val = 255 + (int)b + 1;
        }
        StringBuilder sb = new StringBuilder();
        int n = 128;
        while( n > 0 ){
            if( val >= n){
                sb.append("1");
                val = val - n;
            } else {
                sb.append("0");
            }
            n = n / 2;
        }
        return sb.toString();
    }

    public static String base64Decode(String value) {
        value = cleanBase64(value);
        byte[] dst = getBase64DecodedByteArray(value);
        StringBuilder sb = new StringBuilder();
        for( byte b : dst){
            sb.append(((char) b));
        }
        return sb.toString();
    }

    public static String binaryToString(String buffer, int startIndex, int totalOffset) {
        if( buffer == null)
            return "";

        int length =  buffer.length();

        if (length <= startIndex || length < startIndex + totalOffset) {
            return "";
        }

        StringBuilder total = new StringBuilder();

        for (int i=startIndex; i < (startIndex + totalOffset); i++) {
            total.append(buffer.charAt(i));
        }

        return total.toString();
    }

    public static Integer binaryToNumber(String buffer, int startIndex, int totalOffset) {
        return binaryToDecimalLength(buffer, startIndex, totalOffset); //length
    }

    public static String binaryToLanguage(String buffer, int startIndex, int totalOffset) {
        if( buffer == null)
            return "0";

        int length =  buffer.length();

        if (length <= startIndex || length <= startIndex + totalOffset - 1) {
            return "0";
        }

        StringBuilder language = new StringBuilder();

        String first = getLetter(binaryToDecimalLength(buffer, startIndex, totalOffset - 6));
        language.append(first);

        String second = getLetter(binaryToDecimalLength(buffer, startIndex + 6, totalOffset - 6));
        language.append(second);

        return language.toString();
    }
    
    private static String getLetter( int letterNumber) {
        switch (letterNumber) {
            case 0: return "A";
            case 1: return "B";
            case 2: return "C";
            case 3: return "D";
            case 4: return "E";
            case 5: return "F";
            case 6: return "G";
            case 7: return "H";
            case 8: return "I";
            case 9: return "J";
            case 10: return "K";
            case 11: return "L";
            case 12: return "M";
            case 13: return "N";
            case 14: return "O";
            case 15: return "P";
            case 16: return "Q";
            case 17: return "R";
            case 18: return "S";
            case 19: return "T";
            case 20: return "U";
            case 21: return "V";
            case 22: return "W";
            case 23: return "X";
            case 24: return "Y";
            case 25: return "Z";
        }
        return "";
    }
}
