package net.consentmanager.tvsdk.model.range;

import java.util.ArrayList;
import java.util.List;

public class RangeSection {

    private char defaultConsent;
    private int numEntries;
    private List<RangeEntry> entries;

    public char getDefaultConsent() {
        return defaultConsent;
    }

    public void setDefaultConsent(char defaultConsent) {
        this.defaultConsent = defaultConsent;
    }

    public int getNumEntries() {
        return numEntries;
    }

    public void setNumEntries(int numEntries) {
        this.numEntries = numEntries;
    }

    public List<RangeEntry> getEntries() {
        if (entries == null) {
            entries = new ArrayList<>();
        }

        return entries;
    }

    public void setEntries(List<RangeEntry> entries) {
        this.entries = entries;
    }

    @Override
    public String toString() {
        return "\ndefaultConsent=" + defaultConsent +
                "\nnumEntries=" + numEntries +
                "\nentries=" + entries;
    }
}
