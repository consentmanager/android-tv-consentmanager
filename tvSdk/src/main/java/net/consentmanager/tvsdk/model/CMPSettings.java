package net.consentmanager.tvsdk.model;


import android.content.Context;

import net.consentmanager.tvsdk.storage.CMPPrivateStorage;
import net.consentmanager.tvsdk.storage.CMPStorageV1;
import net.consentmanager.tvsdk.storage.CMPStorageV2;

import java.io.Serializable;

public class CMPSettings implements Serializable {
    private final SubjectToGdpr subjectToGdpr;
    private final String consentToolUrl;

    private CMPSettings(Context context){
        this.subjectToGdpr = getSubjectToGdpr(context);
        this.consentToolUrl = getConsentToolUrl(context);
    }

    public SubjectToGdpr getSubjectToGdpr() {
        return subjectToGdpr;
    }

    public String getConsentToolUrl(){
        return consentToolUrl;
    }

    /**
     * @return Enum that indicates
     * 'CMPGDPRDisabled' – value 0, not subject to GDPR,
     * 'CMPGDPREnabled'  – value 1, subject to GDPR,
     * 'CMPGDPRUnknown'  - value -1, unset.
     */
    public static SubjectToGdpr getSubjectToGdpr(Context context) {
        return CMPStorageV1.getSubjectToGdpr(context);
    }


    /**
     * @param subjectToGdpr Enum that indicates
     *                      'CMPGDPRDisabled' – value 0, not subject to GDPR,
     *                      'CMPGDPREnabled'  – value 1, subject to GDPR,
     *                      'CMPGDPRUnknown'  - value -1, unset.
     */
    public static void setSubjectToGdpr(Context context, SubjectToGdpr subjectToGdpr) {
        if(subjectToGdpr != null) {
            CMPStorageV1.setSubjectToGdpr(context, subjectToGdpr);
            CMPStorageV2.setGDPRApplies(context, subjectToGdpr.getValue().equals("1"));
        }
    }

    /**
     * @return url that is used to create and load the request into the WebView
     */
    public static String getConsentToolUrl(Context context) {
        return CMPPrivateStorage.getConsentToolURL(context);
    }

    /**
     * @param consentToolUrl url that is used to create and load the request into the WebView
     */
    public static void setConsentToolUrl(Context context, String consentToolUrl) {
        CMPPrivateStorage.setConsentToolURL(context, consentToolUrl);
    }

    public static CMPSettings asInstance(Context context) {
        return new CMPSettings(context);
    }
}
