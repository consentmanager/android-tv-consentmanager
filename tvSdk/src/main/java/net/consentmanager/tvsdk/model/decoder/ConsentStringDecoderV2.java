package net.consentmanager.tvsdk.model.decoder;

import android.content.Context;
import android.util.Pair;

import net.consentmanager.tvsdk.model.CMPUtils;
import net.consentmanager.tvsdk.model.PublisherRestriction;
import net.consentmanager.tvsdk.model.PublisherRestrictionType;
import net.consentmanager.tvsdk.storage.CMPStorageV2;

import java.util.LinkedList;
import java.util.List;

public class ConsentStringDecoderV2 {

    static int VERSION_BIT_OFFSET = 0;
    static int VERSION_BIT_LENGTH = 6;

    static int CREATED_BIT_OFFSET = 6;
    static int CREATED_BIT_LENGTH = 36;

    static int LAST_UPDATED_BIT_OFFSET = 42;
    static int LAST_UPDATED_BIT_LENGTH = 36;

    static int CMP_ID_BIT_OFFSET = 78;
    static int CMP_ID_BIT_LENGTH = 12;

    static int CMP_VERSION_BIT_OFFSET = 90;
    static int CMP_VERSION_BIT_LENGTH = 12;

    static int CONSENT_SCREEN_BIT_OFFSET = 102;
    static int CONSENT_SCREEN_BIT_LENGTH = 6;

    static int CONSENT_LANGUAGE_BIT_OFFSET = 108;
    static int CONSENT_LANGUAGE_BIT_LENGTH = 12;

    static int VENDOR_LIST_VERSION_BIT_OFFSET = 120;
    static int VENDOR_LIST_VERSION_BIT_LENGTH = 12;

    static int TCF_POLICY_VERSION_BIT_OFFSET = 132;
    static int TCF_POLICY_VERSION_BIT_LENGTH = 6;

    static int IS_SERVICE_SPECIFIC_BIT_OFFSET = 138;
    static int IS_SERVICE_SPECIFIC_BIT_LENGTH = 1;

    static int USE_NON_STANDARD_STACK_BIT_OFFSET = 139;
    static int USE_NON_STANDARD_STACK_BIT_LENGTH = 1;

    static int SPECIAL_FEATURE_OPT_INS_BIT_OFFSET = 140;
    static int SPECIAL_FEATURE_OPT_INS_BIT_LENGTH = 12;

    static int PURPOSE_CONSENT_BIT_OFFSET = 152;
    static int PURPOSE_CONSENT_BIT_LENGTH = 24;

    static int PURPOSE_LI_TRANSPARENCY_BIT_OFFSET = 176;
    static int PURPOSE_LI_TRANSPARENCY_BIT_LENGTH = 24;

    static int PURPOSE_ONE_TREATMENT_BIT_OFFSET = 200;
    static int PURPOSE_ONE_TREATMENT_BIT_LENGTH = 1;

    static int PUPBLISHER_CC_BIT_OFFSET = 201;
    static int PUPBLISHER_CC_BIT_LENGTH = 12;

    static int MAX_VENDOR_BIT_OFFSET = 213;
    static int MAX_VENDOR_BIT_LENGTH = 16;

    static int MAX_VENDOR_IS_RANGE_ENCODED_BIT_OFFSET = 229;
    static int MAX_VENDOR_IS_RANGE_ENCODED_BIT_LENGTH = 1;

    static int RANGE_ENTRIES_BIT_LENGTH = 12;

    static int NUM_PUB_RESTRICTIONS_BIT_LENGTH = 12;

    static int PURPOSE_ID_BIT_LENGTH = 6;
    static int RESTRICTION_TYPE_BIT_LENGTH = 2;

    static int SEGMENT_TYPE_BIT_OFFSET = 0;
    static int SEGMENT_TYPE_BIT_LENGTH = 3;

    static int PUB_PURPOSE_CONSENTS_BIT_OFFSET = 3;
    static int PUB_PURPOSE_CONSENTS_BIT_LENGTH = 24;

    static int PUB_PURPOSE_LI_TRANSPARENCY_BIT_OFFSET = 27;
    static int PUB_PURPOSE_LI_TRANSPARENCY_BIT_LENGTH = 24;

    static int NUM_CUSTOM_PURPOSES_BIT_OFFSET = 51;
    static int NUM_CUSTOM_PURPOSES_BIT_LENGTH = 6;

    /**
     The SDK Id that is beeing used by the consentString
     */
    private Integer SDKID;

/**
 The Sdk Version that is used by the consentManager
 */
    private Integer SDKVersion;

/**
 If GDPR Applies to this ConsentString
 */
    private Integer gdprApplies;
/**
 Gets the purpose onTreatment from the consentString
 */
    private Integer purposeOneTreatment;
/**
 If the consentString allows non Standard Stacks
 */
    private Integer useNoneStandardStacks;
/**
 The Publischer CC
 */
    private String publisherCC;
/**
 The vendor Consents given in the ConsentString
 */
    private String vendorConsents;
/**
 The Vendor Legitimation Interest given in the ConsentString
 */
    private String vendorLegitimateInterests;
/**
 The Purpose consent given in the ConsentString
 */
    private String purposeConsents;
/**
 The Purpose Legitimation Interest given in the ConsentString
 */
    private String purposeLegitimateInterests;
/**
 The Special feature Opts given in the ConsentString
 */
    private String specialFeaturesOptIns;
/**
 The Publisher Restrictions given in the ConsentString
 */
    private List<PublisherRestriction> publisherRestrictions;
/**
 The Publisher Consnet given in the ConsentString
 */
    private String publisherConsent;
/**
 The Publisher LegitimationInterest in the ConsentString
 */
    private String publisherLegitimateInterests;
/**
 The Publisher Custom Purpose Consnet in the ConsentString
 */
    private String publisherCustomPurposesConsent;
/**
 The Publisher Custom Purpose Legitmation Interest given in the ConsentString
 */
    private String publisherCustomPurposesLegitimateInterests;

/**
 The Disclosed Vendors from DisclosedVendor String as binary String
 */
    private String cmpDisclosedVendors;
/**
 The Allowed Vendors from AllowedVendor String as binary String
 */
    private String cmpAllowedVendors;

/**
 The Allowed Vendors from AllowedVendor String as binary String
 */
    private Integer version;

/**
 The Allowed Vendors from AllowedVendor String as binary String
 */
    private Integer created;

/**
 The Allowed Vendors from AllowedVendor String as binary String
 */
    private Integer lastUpdated;

/**
 The Allowed Vendors from AllowedVendor String as binary String
 */
    private Integer consentScreen;

/**
 The Allowed Vendors from AllowedVendor String as binary String
 */
    private String consentLanguage;

/**
 The Allowed Vendors from AllowedVendor String as binary String
 */
    private Integer vendorListVersion;

/**
 The Allowed Vendors from AllowedVendor String as binary String
 */
    private Integer isServiceSpecific;

/**
 The Policy Version of the ConsentString
 */
    private Integer policyVersion;

    public void processConsentString(Context context, String consentString) {
        CMPStorageV2.setTCString(context, consentString);
        parse(consentString, context);
    }


    private void parse( String consentString,  Context context) {
        String[] splits = consentString.split("\\.");

        String coreString = splits[0];
        String disclosedVendors = null;
        String allowedVendors = null;
        String publisherTC = null;

        for(int i = 1; i < splits.length; i++ ){
            String buffer = CMPUtils.binaryConsentFrom(CMPUtils.cleanBase64ForBinary(splits[i]));
            if (buffer == null) {
                return;
            }
            Integer segmentType = CMPUtils.binaryToDecimalLength(buffer , SEGMENT_TYPE_BIT_OFFSET, SEGMENT_TYPE_BIT_LENGTH);
            switch (segmentType) {
                case 1:
                    disclosedVendors = splits[i];
                    break;
                case 2:
                    allowedVendors = splits[i];
                    break;
                case 3:
                    publisherTC = splits[i];
                    break;
                default:
                    break;
            }
        }
        String buffer = CMPUtils.binaryConsentFrom(CMPUtils.cleanBase64ForBinary(coreString));
        if (buffer == null) {
            return;
        }
        
        Integer version = CMPUtils.binaryToDecimalLength(buffer, VERSION_BIT_OFFSET, VERSION_BIT_LENGTH);

        Integer created = CMPUtils.binaryToDecimalLength(buffer, CREATED_BIT_OFFSET, CREATED_BIT_LENGTH);

        Integer lastUpdated = CMPUtils.binaryToDecimalLength(buffer, LAST_UPDATED_BIT_OFFSET, LAST_UPDATED_BIT_LENGTH);

        Integer SDKID = CMPUtils.binaryToDecimalLength(buffer, CMP_ID_BIT_OFFSET, CMP_ID_BIT_LENGTH);

        Integer SDKVersion = CMPUtils.binaryToDecimalLength(buffer, CMP_VERSION_BIT_OFFSET, CMP_VERSION_BIT_LENGTH);

        Integer consentScreen = CMPUtils.binaryToDecimalLength(buffer, CONSENT_SCREEN_BIT_OFFSET, CONSENT_SCREEN_BIT_LENGTH);

        String consentLanguage = CMPUtils.binaryToLanguage(buffer, CONSENT_LANGUAGE_BIT_OFFSET, CONSENT_LANGUAGE_BIT_LENGTH);

        Integer vendorListVersion = CMPUtils.binaryToDecimalLength(buffer, VENDOR_LIST_VERSION_BIT_OFFSET, VENDOR_LIST_VERSION_BIT_LENGTH);

        Integer policyVersion = CMPUtils.binaryToDecimalLength(buffer, TCF_POLICY_VERSION_BIT_OFFSET, TCF_POLICY_VERSION_BIT_LENGTH);

        Integer isServiceSpecific = CMPUtils.binaryToDecimalLength(buffer, IS_SERVICE_SPECIFIC_BIT_OFFSET, IS_SERVICE_SPECIFIC_BIT_LENGTH);

        Integer useNoneStandardStacks = CMPUtils.binaryToDecimalLength(buffer, USE_NON_STANDARD_STACK_BIT_OFFSET, USE_NON_STANDARD_STACK_BIT_LENGTH);

        String specialFeaturesOptIns = CMPUtils.binaryToString(buffer, SPECIAL_FEATURE_OPT_INS_BIT_OFFSET, SPECIAL_FEATURE_OPT_INS_BIT_LENGTH);

        String purposeConsents = CMPUtils.binaryToString(buffer, PURPOSE_CONSENT_BIT_OFFSET, PURPOSE_CONSENT_BIT_LENGTH);

        String purposeLegitimateInterests = CMPUtils.binaryToString(buffer, PURPOSE_LI_TRANSPARENCY_BIT_OFFSET, PURPOSE_LI_TRANSPARENCY_BIT_LENGTH);

        Integer purposeOneTreatment = CMPUtils.binaryToDecimalLength(buffer, PURPOSE_ONE_TREATMENT_BIT_OFFSET, PURPOSE_ONE_TREATMENT_BIT_LENGTH);

        String publisherCC = CMPUtils.binaryToLanguage(buffer, PUPBLISHER_CC_BIT_OFFSET, PUPBLISHER_CC_BIT_LENGTH);

        Integer maxVendor = CMPUtils.binaryToDecimalLength(buffer, MAX_VENDOR_BIT_OFFSET, MAX_VENDOR_BIT_LENGTH);

        Integer isRangeEncoded = CMPUtils.binaryToDecimalLength(buffer, MAX_VENDOR_IS_RANGE_ENCODED_BIT_OFFSET, MAX_VENDOR_IS_RANGE_ENCODED_BIT_LENGTH);

        int offset = MAX_VENDOR_IS_RANGE_ENCODED_BIT_OFFSET + MAX_VENDOR_IS_RANGE_ENCODED_BIT_LENGTH;

        if( isRangeEncoded == 0){
            vendorConsents = CMPUtils.binaryToString(buffer, offset, maxVendor);
            offset += maxVendor;
        } else {
            Pair<String,Integer> p = extractRangeFieldSection(buffer, offset);
            vendorConsents =  p.first;
            offset = p.second;
        }


        maxVendor = CMPUtils.binaryToDecimalLength(buffer, offset, MAX_VENDOR_BIT_LENGTH);

        offset += MAX_VENDOR_BIT_LENGTH;
        isRangeEncoded = CMPUtils.binaryToDecimalLength(buffer, offset, MAX_VENDOR_IS_RANGE_ENCODED_BIT_LENGTH);

        offset += MAX_VENDOR_IS_RANGE_ENCODED_BIT_LENGTH;

        if( isRangeEncoded == 0){
            vendorLegitimateInterests = CMPUtils.binaryToString(buffer, offset, maxVendor);
            offset += maxVendor;
        } else {
            Pair<String,Integer> p = extractRangeFieldSection(buffer, offset);
            vendorLegitimateInterests =  p.first;
            offset = p.second;
        }

        Integer numPubRestrictions = CMPUtils.binaryToDecimalLength(buffer, offset, NUM_PUB_RESTRICTIONS_BIT_LENGTH);

        offset += NUM_PUB_RESTRICTIONS_BIT_LENGTH;
        List<PublisherRestriction> pRestrictions = new LinkedList<>();

        for(int i = 0; i < numPubRestrictions; i++){
            Integer purposeId = CMPUtils.binaryToDecimalLength(buffer, offset, PURPOSE_ID_BIT_LENGTH);

            offset += PURPOSE_ID_BIT_LENGTH;

            Integer restrictionType = CMPUtils.binaryToDecimalLength(buffer, offset, RESTRICTION_TYPE_BIT_LENGTH);

            offset += RESTRICTION_TYPE_BIT_LENGTH;
            Pair<String,Integer> p = extractRangeFieldSection(buffer, offset);
            String vendorIds =  p.first;
            offset = p.second;

            int index = indexOfPurpose(purposeId, pRestrictions);

            PublisherRestrictionType rtv = new PublisherRestrictionType(restrictionType, vendorIds);
            if( index < 0){
                pRestrictions.add(new PublisherRestriction(purposeId, rtv));
            } else {
                pRestrictions.get(index).addRestrictionType(rtv);
            }

        }

        publisherRestrictions = pRestrictions;

        if( allowedVendors != null){
            buffer = CMPUtils.binaryConsentFrom(allowedVendors);

            if (buffer == null) {
                return;
            }
            offset = Integer.valueOf(3);
            maxVendor = CMPUtils.binaryToDecimalLength(buffer, offset, MAX_VENDOR_BIT_LENGTH);
            offset+= MAX_VENDOR_BIT_LENGTH;
            isRangeEncoded = CMPUtils.binaryToDecimalLength(buffer, offset, MAX_VENDOR_IS_RANGE_ENCODED_BIT_LENGTH);
            offset += MAX_VENDOR_IS_RANGE_ENCODED_BIT_LENGTH;

            if( isRangeEncoded == 0){
                cmpAllowedVendors = CMPUtils.binaryToString(buffer, offset, maxVendor);
            } else {
                Pair<String,Integer> p = extractRangeFieldSection(buffer, offset);
                cmpAllowedVendors = p.first;
            }
        }

        if( disclosedVendors != null){
            buffer = CMPUtils.binaryConsentFrom(disclosedVendors);

            if (buffer == null) {
                return;
            }

            offset = Integer.valueOf(3);
            maxVendor = CMPUtils.binaryToDecimalLength(buffer, offset, MAX_VENDOR_BIT_LENGTH);
            offset+= MAX_VENDOR_BIT_LENGTH;
            isRangeEncoded = CMPUtils.binaryToDecimalLength(buffer, offset, MAX_VENDOR_IS_RANGE_ENCODED_BIT_LENGTH);
            offset += MAX_VENDOR_IS_RANGE_ENCODED_BIT_LENGTH;

            if( isRangeEncoded == 0){
                cmpDisclosedVendors = CMPUtils.binaryToString(buffer, offset, maxVendor);
            } else {
                Pair<String,Integer> p = extractRangeFieldSection(buffer, offset);
                cmpDisclosedVendors = p.first;
            }
        }

        if( publisherTC != null){
            buffer = CMPUtils.binaryConsentFrom(disclosedVendors);

            if (buffer == null) {
                return;
            }
            publisherConsent = CMPUtils.binaryToString(buffer, PUB_PURPOSE_CONSENTS_BIT_OFFSET, PUB_PURPOSE_CONSENTS_BIT_LENGTH);
            publisherLegitimateInterests = CMPUtils.binaryToString(buffer, PUB_PURPOSE_LI_TRANSPARENCY_BIT_OFFSET, PUB_PURPOSE_LI_TRANSPARENCY_BIT_LENGTH);

            int numCustomPupose = CMPUtils.binaryToDecimalLength(buffer, NUM_CUSTOM_PURPOSES_BIT_OFFSET, NUM_CUSTOM_PURPOSES_BIT_LENGTH);

            offset = NUM_CUSTOM_PURPOSES_BIT_OFFSET + NUM_CUSTOM_PURPOSES_BIT_LENGTH;
            publisherCustomPurposesConsent = CMPUtils.binaryToString(buffer, offset, numCustomPupose);

            offset += numCustomPupose;
            publisherCustomPurposesLegitimateInterests = CMPUtils.binaryToString(buffer, offset, numCustomPupose);
        }

        CMPStorageV2.setSDKID(context,SDKID);
        CMPStorageV2.setSDKVersion(context,SDKVersion);
        CMPStorageV2.setPurposeOneTreatment(context,purposeOneTreatment == 1);
        CMPStorageV2.setUseNonStandardStacks(context,useNoneStandardStacks == 1);
        CMPStorageV2.setPublisherCC(context,publisherCC);
        CMPStorageV2.setVendorConsents(context,vendorConsents);
        CMPStorageV2.setVendorLegitimateInterests(context,vendorLegitimateInterests);
        CMPStorageV2.setPurposeConsents(context,purposeConsents);
        CMPStorageV2.setPurposeLegitimateInterests(context,purposeLegitimateInterests);
        CMPStorageV2.setSpecialFeaturesOptIns(context,specialFeaturesOptIns);
        CMPStorageV2.setPublisherRestrictions(context,publisherRestrictions);
        CMPStorageV2.setPublisherConsent(context,publisherConsent);
        CMPStorageV2.setPurposeLegitimateInterests(context,purposeLegitimateInterests);
        CMPStorageV2.setPublisherCustomPurposesConsents(context,publisherCustomPurposesConsent);
        CMPStorageV2.setPublisherCustomPurposesLegitimateInterests(context,publisherCustomPurposesLegitimateInterests);
        CMPStorageV2.setPolicyVersion(context,policyVersion);
        CMPStorageV2.commit();

    }

    private int indexOfPurpose( int purposeId, List<PublisherRestriction> prArray){
        for( int i = 0; i < prArray.size(); i++){
            if( prArray.get(i).getPurposeId() == purposeId ){
                return i;
            }
        }
        return -1;
    }

    private Pair<String, Integer> extractRangeFieldSection(String buffer, int startIndex){

        Integer entries = CMPUtils.binaryToDecimalLength(buffer, startIndex,RANGE_ENTRIES_BIT_LENGTH);
        startIndex += RANGE_ENTRIES_BIT_LENGTH;

        StringBuilder value = new StringBuilder();
        for( int i = 0; i < entries; i++){
            int isARange = CMPUtils.binaryToDecimalLength(buffer, startIndex, 1);
            startIndex += 1;
            int startOrOnlyVendorId = CMPUtils.binaryToDecimalLength(buffer, startIndex, 16);
            
            startIndex += 16;
            if(isARange == 1){
                int endVendorId = CMPUtils.binaryToDecimalLength(buffer, startIndex, 16);

                //Its possible to catch Errors in Consent String here
                startIndex += 16;
                value = setBitRangeExtension(value, startOrOnlyVendorId, endVendorId);

            } else {
                value.append(getBitExtension(value.length(), startOrOnlyVendorId));
            }
        }

        return new Pair<>(value.toString(), startIndex);
    }

    private StringBuilder getBitExtension(int fromIndex, int toIndex){
        StringBuilder extract = new StringBuilder();
        for( int i = fromIndex; i < toIndex - 1; i++ ){
            extract.append("0");
        }
        extract.append("1");
        return extract;
    }

    private StringBuilder setBitRangeExtension(StringBuilder value, int fromIndex, int toIndex){

        StringBuilder extract = new StringBuilder();
        if( value.length() <= fromIndex){
            extract.append( getBitExtension(value.length(), fromIndex));
        }

        for( int i = fromIndex; i <= toIndex; i++ ){
            extract.append("1");
        }

        if( value.length() <= fromIndex){
            return extract;
        }
        return placeBitExtension(extract.toString(), value, fromIndex);
    }

    private StringBuilder placeBitExtension(String extract, StringBuilder value, int atIndex){
        while(value.length() < atIndex + extract.length()){
            value.append("0");
        }
        return value.replace(atIndex,atIndex + extract.length(), extract);
    }



}
