package net.consentmanager.tvsdk.storage;

import android.content.Context;
import android.os.StrictMode;
import android.preference.PreferenceManager;

import net.consentmanager.tvsdk.model.CMPUtils;

import java.util.Date;

public class CMPPrivateStorage extends CMPStorage{
    //private Data to Save
    private static final String CONSENT_TOOL_URL = "IABConsent_ConsentToolUrl";
    private static final String CMP_REQUEST = "IABConsent_CMPRequest";
    private static final String CMP_ACCEPTANCE = "IABConsent_CMPAcceptance";

    /**
     * Stores the passed date millis as a string in the shared preferences
     *
     * @param context  Context used to access the SharedPreferences
     * @param date the Date, when the Server was last requested
     */
    public static void setLastRequested(Context context, Date date) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        if( date != null) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString(CMP_REQUEST, "" + date.getTime()).apply();
        } else {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString(CMP_REQUEST, null).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Returns the date when the server was requested for the last time
     *
     * @param context Context used to access the SharedPreferences
     * @return the stored Date
     */
    public static Date getLastRequested(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        String value = PreferenceManager.getDefaultSharedPreferences(context).getString(CMP_REQUEST, EMPTY_DEFAULT_STRING);
        CMPUtils.resetStrictMode(oldThreadPolicy);
        if( value == null || value.equals(EMPTY_DEFAULT_STRING)){
            return null;
        } else {
            long time = Long.parseLong(value);
            return new Date(time);
        }
    }

    /**
     * Stores if a consent of the user is needed
     *
     * @param context  Context used to access the SharedPreferences
     * @param needs If the consent is needed.
     */
    public static void setNeedsAcceptance(Context context, Boolean needs) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        if( needs != null) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(CMP_ACCEPTANCE, needs ? 1 : 0).apply();
        } else {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString(CMP_ACCEPTANCE, null).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Returns the date when the consentScreen was accepted for the last time
     *
     * @param context Context used to access the SharedPreferences
     * @return the stored Date
     */
    public static boolean needsAcceptance(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        Integer value = PreferenceManager.getDefaultSharedPreferences(context).getInt(CMP_ACCEPTANCE, EMPTY_DEFAULT_INT);
        CMPUtils.resetStrictMode(oldThreadPolicy);
        return value.equals(1);
    }

    /**
     * Returns the ConsentToolURL as a String.
     * @param context The context of the app View
     * @return The ConsentToolURL
     */
    public static String getConsentToolURL(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(CONSENT_TOOL_URL, EMPTY_DEFAULT_STRING));
    }

    /**
     * Sets a new ConsentToolURL .
     * @param context The context of the app View
     * @param url The Url, which should be shown.
     */
    public static void setConsentToolURL(Context context, String url) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(CONSENT_TOOL_URL, url).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Resets all Shared Preferences saved by this instance.
     * @param context The context of the app View
     */
    public static void clearConsents(Context context) {
        setConsentToolURL(context, null);
        setNeedsAcceptance(context, null);
        setLastRequested(context, null);
    }
}
