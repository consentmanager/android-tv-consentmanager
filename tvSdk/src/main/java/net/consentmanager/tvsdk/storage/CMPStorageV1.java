package net.consentmanager.tvsdk.storage;

import net.consentmanager.tvsdk.model.CMPUtils;
import net.consentmanager.tvsdk.model.SubjectToGdpr;

import android.content.Context;
import android.os.StrictMode;
import android.preference.PreferenceManager;

/**
 * Used to retrieve and store the consentData, subjectToGdpr, vendors, purposes, consentToolUrl in the SharedPreferences
 */
public class CMPStorageV1  extends CMPStorage implements StorageInterface{
    //public Data to Save CMP V1
    private static final String CONSENT_STRING = "IABConsent_ConsentString";
    private static final String SUBJECT_TO_GDPR = "IABConsent_SubjectToGDPR"; //regulation
    private static final String CMP_PRESENT = "IABConsent_CMPPresent"; // if screen is open
    private static final String VENDORS = "IABConsent_ParsedVendorConsents";
    private static final String PURPOSES = "IABConsent_ParsedPurposeConsents";

    /**
     * Returns the web safe base64-encoded consent String stored in the SharedPreferences
     *
     * @param context Context used to access the SharedPreferences
     * @return the stored web safe base64-encoded consent String
     */
    public static String getConsentString(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        String consentString = "";
        if( PreferenceManager.getDefaultSharedPreferences(context).contains(CONSENT_STRING)) {
            consentString = PreferenceManager.getDefaultSharedPreferences(context).getString(CONSENT_STRING, EMPTY_DEFAULT_STRING);
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
        return consentString;
    }

    /**
     * Stores the passed web safe base64-encoded consent String in the SharedPreferences
     *
     * @param context       Context used to access the SharedPreferences
     * @param consentString the web safe base64-encoded consent String to be stored
     */
    public static void setConsentString(Context context, String consentString) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(CONSENT_STRING, consentString).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Returns the {@link SubjectToGdpr} stored in the SharedPreferences
     *
     * @param context Context used to access the SharedPreferences
     * @return the stored {@link SubjectToGdpr}
     */
    public static SubjectToGdpr getSubjectToGdpr(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
                String subjectToGdpr = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SUBJECT_TO_GDPR, SubjectToGdpr.CMPGDPRUnknown.getValue());
        CMPUtils.resetStrictMode(oldThreadPolicy);
        return SubjectToGdpr.getValueForString(subjectToGdpr);
    }

    /**
     * Stores the passed {@link SubjectToGdpr} in the SharedPreferences
     *
     * @param context       Context used to access the SharedPreferences
     * @param subjectToGdpr the {@link SubjectToGdpr} to be stored
     */
    public static void setSubjectToGdpr(Context context, SubjectToGdpr subjectToGdpr) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        String subjectToGdprValue = null;

        if (subjectToGdpr == SubjectToGdpr.CMPGDPRDisabled || subjectToGdpr == SubjectToGdpr.CMPGDPREnabled) {
            subjectToGdprValue = subjectToGdpr.getValue();
        }

        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(SUBJECT_TO_GDPR, subjectToGdprValue).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Returns the CMP present boolean stored in the SharedPreferences
     *
     * @return {@code true} if a CMP implementing the iAB specification is present in the application, otherwise {@code false};
     */
    public static boolean getCmpPresentValue(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getBoolean(CMP_PRESENT, false));
    }

    /**
     * Stores the CMP present boolean in SharedPreferences
     *
     * @param cmpPresent indicates whether a CMP implementing the iAB specification is present in the application
     */
    public static void setCmpPresentValue(Context context, boolean cmpPresent) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(CMP_PRESENT, cmpPresent).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Returns the vendors binary String stored in the SharedPreferences
     *
     * @param context Context used to access the SharedPreferences
     * @return the stored vendors binary String
     */
    public static String getVendorsString(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(VENDORS, EMPTY_DEFAULT_STRING));
    }

    /**
     * Stores the passed vendors binary String in SharedPreferences
     *
     * @param context Context used to access the SharedPreferences
     * @param vendors binary String to be stored
     */
    public static void setVendorsString(Context context, String vendors) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(VENDORS, vendors).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Returns the purposes binary String stored in the SharedPreferences
     *
     * @param context Context used to access the SharedPreferences
     * @return the stored purposes binary String
     */
    public static String getPurposesString(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(PURPOSES, EMPTY_DEFAULT_STRING));
    }

    /**
     * Stores the passed purposes binary String in SharedPreferences
     *
     * @param context  Context used to access the SharedPreferences
     * @param purposes binary String to be stored
     */
    public static void setPurposesString(Context context, String purposes) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(PURPOSES, purposes).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Returns whether the consent was given for the passed purpose id
     *
     * @param context   Context used to access the SharedPreferences
     * @param purposeId the purpose id to check the consent for
     * @return true if consent was given, false otherwise
     */
    public static boolean isPurposeConsentGivenForPurposeId(Context context, int purposeId) {
        String purposes = getPurposesString(context);
        return purposes != null &&  purposes.length() >= purposeId && purposes.charAt(purposeId - 1) == '1';
    }

    /**
     * Returns whether the consent was given for the passed vendor id
     *
     * @param context  Context used to access the SharedPreferences
     * @param vendorId the vendor id to check the consent for
     * @return true if consent was given, false otherwise
     */
    public static boolean isVendorConsentGivenForVendorId(Context context, int vendorId) {
        String vendors = getVendorsString(context);
        return vendors != null && vendors.length() >= vendorId && vendors.charAt(vendorId - 1) == '1';
    }

    /**
     * Clears the Storage
     * @param context The Context of the App.
     */
    public static void clearConsents(Context context){
        setConsentString(context, null);
        setVendorsString(context, null);
        setPurposesString(context, null);
        setSubjectToGdpr(context, null);
        setCmpPresentValue(context, false);
    }
}