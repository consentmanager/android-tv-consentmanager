package net.consentmanager.tvsdk.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;

import net.consentmanager.tvsdk.CMPConsentTool;
import net.consentmanager.tvsdk.model.CMPUtils;
import net.consentmanager.tvsdk.model.PublisherRestriction;

import java.util.LinkedList;
import java.util.List;

/**
 * Used to retrieve and store the consentData, subjectToGdpr, vendors, purposes, consentToolUrl in the SharedPreferences
 */
public class CMPStorageV2  extends CMPStorage implements StorageInterface{
    //public Data to Save CMP V2
    private static final String CMP_SDK_ID = "IABTCF_CmpSdkID";
    private static final String CMP_SDK_VERSION = "IABTCF_CmpSdkVersion";
    private static final String POLICY_VERSION = "IABTCF_PolicyVersion";
    private static final String GDPR_APPLIES = "IABTCF_gdprApplies";
    private static final String PUBLISHER_CC = "IABTCF_PublisherCC";
    private static final String TC_STRING = "IABTCF_TCString";
    private static final String VENDOR_CONSENTS = "IABTCF_VendorConsents";
    private static final String VENDOR_LEGITIMATE_INTERESTS = "IABTCF_VendorLegitimateInterests";
    private static final String PURPOSE_CONSENTS= "IABTCF_PurposeConsents";
    private static final String PURPOSE_LEGITIMATE_INTERESTS = "IABTCF_PurposeLegitimateInterests";
    private static final String SPECIAL_FEATURES_OPT_INS = "IABTCF_SpecialFeaturesOptIns";
    private static final String PUBLISHER_RESTRICTIONS = "IABTCF_PublisherRestrictions%d"; //%d = Purpose ID
    private static final String PUBLISHER_CONSENT = "IABTCF_PublisherConsent";
    private static final String PUBLISHER_LEGITIMATE_INTERESTS = "IABTCF_PublisherLegitimateInterests";
    private static final String PUBLISHER_CUSTOM_PURPOSES_CONSENTS = "IABTCF_PublisherCustomPurposesConsents";
    private static final String PUBLISHER_CUSTOM_PURPOSES_LEGITIMATE_INTERESTS = "IABTCF_PublisherCustomPurposesLegitimateInterests";
    private static final String PURPOSE_ONE_TREATMENT = "IABTCF_PurposeOneTreatment";
    private static final String USE_NONE_STANDARD_STACKS = "IABTCF_UseNonStandardStacks";

    private static SharedPreferences.Editor editor = null;

    /**
     * Stores the passed websafe base64-encoded tc String in the SharedPreferences
     *
     * @param context       Context used to access the SharedPreferences
     * @param tcString      the web safe  base64-encoded tc String to be stored
     */
    public static void setTCString(Context context, String tcString) {
        CMPConsentTool.log("Saving Consent TCString: " + tcString);
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( tcString != null) {
            getEditor(context).putString(TC_STRING, tcString).apply();
        } else {
            getEditor(context).remove(TC_STRING).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setPublisherCC(Context context, String publisherCC) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( publisherCC != null) {
            getEditor(context).putString(PUBLISHER_CC, publisherCC).apply();
        } else {
            getEditor(context).remove(PUBLISHER_CC).apply();
        }

        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setPolicyVersion(Context context, Integer tcfPolicyVersion) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( tcfPolicyVersion != null) {
            getEditor(context).putInt(POLICY_VERSION, tcfPolicyVersion).apply();
        } else {
            getEditor(context).remove(POLICY_VERSION).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setSDKVersion(Context context, Integer cmpVersion) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( cmpVersion != null) {
            getEditor(context).putInt(CMP_SDK_VERSION, cmpVersion).apply();
        } else {
            getEditor(context).remove(CMP_SDK_VERSION).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setSDKID(Context context, Integer cmpId) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( cmpId != null) {
            getEditor(context).putInt(CMP_SDK_ID, cmpId).apply();
        } else {
            getEditor(context).remove(CMP_SDK_ID).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setVendorConsents(Context context, String vendorConsent) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( vendorConsent != null) {
            getEditor(context).putString(VENDOR_CONSENTS, vendorConsent).apply();
        } else {
            getEditor(context).remove(VENDOR_CONSENTS).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setVendorLegitimateInterests(Context context, String vendorLegitimateInterest) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( vendorLegitimateInterest != null) {
            getEditor(context).putString(VENDOR_LEGITIMATE_INTERESTS, vendorLegitimateInterest).apply();
        } else {
            getEditor(context).remove(VENDOR_LEGITIMATE_INTERESTS).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setPurposeConsents(Context context, String purposesConsent) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( purposesConsent != null) {
            getEditor(context).putString(PURPOSE_CONSENTS, purposesConsent).apply();
        } else {
            getEditor(context).remove(PURPOSE_CONSENTS).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setPurposeLegitimateInterests(Context context, String purposesLITransparency) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( purposesLITransparency != null) {
            getEditor(context).putString(PURPOSE_LEGITIMATE_INTERESTS,purposesLITransparency).apply();
        } else {
            getEditor(context).remove(PURPOSE_LEGITIMATE_INTERESTS).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setSpecialFeaturesOptIns(Context context, String specialFeatureOptIns) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( specialFeatureOptIns != null) {
            getEditor(context).putString(SPECIAL_FEATURES_OPT_INS, specialFeatureOptIns).apply();
        } else {
            getEditor(context).remove(SPECIAL_FEATURES_OPT_INS).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }


    public static void setPublisherRestrictions(Context context, List<PublisherRestriction> publisherRestrictions) {
        for( int i = 0; i < publisherRestrictions.size(); i++){
            setPublisherRestriction(context, publisherRestrictions.get(i), publisherRestrictions.get(i).getPurposeId());
        }
    }

    public static void setPublisherRestriction(Context context, PublisherRestriction publisherRestriction, int purposeId){
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( publisherRestriction != null){
            getEditor(context).putString(String.format(PUBLISHER_RESTRICTIONS, purposeId), publisherRestriction.getVendorsAsNSUSerDefaultsString()).apply();
        } else {
            getEditor(context).remove(String.format(PUBLISHER_RESTRICTIONS, purposeId)).apply();
        }
    }



    public static void setPublisherRestriction(Context context, int id, String vendorIds) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( vendorIds != null){
            getEditor(context).putString(String.format(PUBLISHER_RESTRICTIONS, id), vendorIds).apply();
        } else {
            getEditor(context).remove(String.format(PUBLISHER_RESTRICTIONS, id)).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setPublisherConsent(Context context, String pubPurposesConsent) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( pubPurposesConsent != null) {
            getEditor(context).putString(PUBLISHER_CONSENT, pubPurposesConsent).apply();
        } else {
            getEditor(context).remove(PUBLISHER_CONSENT).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setPublisherLegitimateInterests(Context context, String pubPurposesLITransparency) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( pubPurposesLITransparency != null) {
            getEditor(context).putString(PUBLISHER_LEGITIMATE_INTERESTS, pubPurposesLITransparency).apply();
        } else {
            getEditor(context).remove(PUBLISHER_LEGITIMATE_INTERESTS).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setPublisherCustomPurposesConsents(Context context, String customPurposesConsent) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( customPurposesConsent != null) {
            getEditor(context).putString(PUBLISHER_CUSTOM_PURPOSES_CONSENTS, customPurposesConsent).apply();
        } else {
            getEditor(context).remove(PUBLISHER_CUSTOM_PURPOSES_CONSENTS).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setPublisherCustomPurposesLegitimateInterests(Context context, String customPurposesLITransparency) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        if( customPurposesLITransparency != null) {
            getEditor(context).putString(PUBLISHER_CUSTOM_PURPOSES_LEGITIMATE_INTERESTS, customPurposesLITransparency).apply();
        } else {
            getEditor(context).remove(PUBLISHER_CUSTOM_PURPOSES_LEGITIMATE_INTERESTS).apply();
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }


    public static void setUseNonStandardStacks(Context context, boolean useNonStandardStacks) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        getEditor(context).putInt(USE_NONE_STANDARD_STACKS, useNonStandardStacks ? 1 : 0).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setPurposeOneTreatment(Context context, boolean purposeOneTreatment) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        getEditor(context).putInt(PURPOSE_ONE_TREATMENT, purposeOneTreatment ? 1 : 0).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static void setGDPRApplies(Context context, boolean applies) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReadsWrites();
        getEditor(context).putInt(GDPR_APPLIES, applies ? 1 : 0).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Returns the web safe  base64-encoded tc String stored in the SharedPreferences
     *
     * @param context Context used to access the SharedPreferences
     * @return the stored web safe  base64-encoded tc String
     */
    public static String getTCString(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(TC_STRING, EMPTY_DEFAULT_STRING));
    }

    public static String getPublisherCC(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(PUBLISHER_CC, EMPTY_DEFAULT_STRING));
    }

    public static int getPolicyVersion(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getInt(POLICY_VERSION, EMPTY_DEFAULT_INT));
    }

    public static int getSDKVersion(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getInt(CMP_SDK_VERSION, EMPTY_DEFAULT_INT));
    }

    public static int getSDKID(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getInt(CMP_SDK_ID, EMPTY_DEFAULT_INT));
    }

    public static String getVendorConsents(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(VENDOR_CONSENTS, EMPTY_DEFAULT_STRING));
    }

    public static String getVendorLegitimateInterests(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(VENDOR_LEGITIMATE_INTERESTS, EMPTY_DEFAULT_STRING));
    }

    public static String getPurposeConsents(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(PURPOSE_CONSENTS, EMPTY_DEFAULT_STRING));
    }

    public static String getPurposeLegitimateInterests(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(PURPOSE_LEGITIMATE_INTERESTS, EMPTY_DEFAULT_STRING));
    }

    public static String getSpecialFeaturesOptIns(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(SPECIAL_FEATURES_OPT_INS, EMPTY_DEFAULT_STRING));
    }

    public static PublisherRestriction getPublisherRestriction(Context context, int id) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        String rString = CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(String.format(PUBLISHER_RESTRICTIONS, id), EMPTY_DEFAULT_STRING));
        if( rString != EMPTY_DEFAULT_STRING) {
            return new PublisherRestriction( id, rString);
        } else {
            return null;
        }
    }

    public static List<PublisherRestriction> getPublisherRestrictions(Context context) {
        int i = 0;
        List<PublisherRestriction> pRestrictions = new LinkedList<>();

        while( getPublisherRestriction(context, i) != null ){
            pRestrictions.add(getPublisherRestriction(context, i));
            i++;
        }
        return pRestrictions;
    }


    public static String getPublisherConsent(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(PUBLISHER_CONSENT, EMPTY_DEFAULT_STRING));
    }

    public static String getPublisherLegitimateInterests(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(PUBLISHER_LEGITIMATE_INTERESTS, EMPTY_DEFAULT_STRING));
    }

    public static String getPublisherCustomPurposesConsents(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(PUBLISHER_CUSTOM_PURPOSES_CONSENTS, EMPTY_DEFAULT_STRING));
    }

    public static String getPublisherCustomPurposesLegitimateInterests(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(PUBLISHER_CUSTOM_PURPOSES_LEGITIMATE_INTERESTS, EMPTY_DEFAULT_STRING));
    }

    public static int getUseNonStandardStacks(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getInt(USE_NONE_STANDARD_STACKS, EMPTY_DEFAULT_INT));
    }

    public static int getPurposeOneTreatment(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getInt(PURPOSE_ONE_TREATMENT, EMPTY_DEFAULT_INT));
    }

    public static int getGDPRApplies(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getInt(GDPR_APPLIES, EMPTY_DEFAULT_INT));
    }


    /**
     * Clears all consent set by this class
     * @param context The context of the app View
     */
    public static void clearConsents(Context context) {
        int i = 1;
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        while( PreferenceManager.getDefaultSharedPreferences(context).contains(String.format(PUBLISHER_RESTRICTIONS, i))) {
            setPublisherRestriction(context, i, null);
            i++;
        }
        CMPUtils.resetStrictMode(oldThreadPolicy);
        setTCString(context, null);
        setPublisherCC(context, null);
        setPolicyVersion(context, null);
        setSDKVersion(context, null);
        setSDKID(context, null);
        setVendorConsents(context, null);
        setVendorLegitimateInterests(context, null);
        setPurposeConsents(context, null);
        setPurposeLegitimateInterests(context, null);
        setSpecialFeaturesOptIns(context, null);
        setPublisherConsent(context, null);
        setPublisherLegitimateInterests(context, null);
        setPublisherCustomPurposesConsents(context, null);
        setPublisherCustomPurposesLegitimateInterests(context, null);
        setPurposeOneTreatment(context, false);
        setGDPRApplies(context, false);
        setUseNonStandardStacks(context, false);
        commit();
    }

    private static SharedPreferences.Editor getEditor(Context context){
        if(editor == null){
            editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        }
        return editor;
    }

    public static void commit(){
        if(editor != null){
            editor.apply();
            editor = null;
        }

    }

}