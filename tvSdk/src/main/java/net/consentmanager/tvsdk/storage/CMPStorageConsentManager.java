package net.consentmanager.tvsdk.storage;

import android.content.Context;
import android.os.StrictMode;
import android.preference.PreferenceManager;

import net.consentmanager.tvsdk.model.CMPUtils;

/**
 * Used to retrieve and store the consentData, subjectToGdpr, vendors, purposes, consentToolUrl in the SharedPreferences
 */
public class CMPStorageConsentManager extends CMPStorage{
    //public Data to Public Storage
    private static final String US_PRIVACY = "IABUSPrivacy_String";
    private static final String VENDORS = "CMConsent_ParsedVendorConsents";
    private static final String PURPOSES = "CMConsent_ParsedPurposeConsents";
    private static final String CONSENT_STRING = "CMConsent_ConsentString";
    private static final String GOOGLE_AC = "IABTCF_AddtlConsent";


    public static String getUSPrivacyString(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(US_PRIVACY, EMPTY_DEFAULT_STRING));
    }

    public static void setUSPrivacyString(Context context, String usPrivacy) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(US_PRIVACY, usPrivacy).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    public static String getConsentString(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(CONSENT_STRING, EMPTY_DEFAULT_STRING));
    }

    public static void setConsentString(Context context, String consent) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(CONSENT_STRING, consent).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Returns the vendors binary String stored in the SharedPreferences
     *
     * @param context Context used to access the SharedPreferences
     * @return the stored vendors binary String
     */
    public static String getVendorsString(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(VENDORS, EMPTY_DEFAULT_STRING));
    }

    /**
     * Stores the passed vendors binary String in SharedPreferences
     *
     * @param context Context used to access the SharedPreferences
     * @param vendors binary String to be stored
     */
    public static void setVendorsString(Context context, String vendors) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(VENDORS, vendors).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Returns the purposes binary String stored in the SharedPreferences
     *
     * @param context Context used to access the SharedPreferences
     * @return the stored purposes binary String
     */
    public static String getPurposesString(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(PURPOSES, EMPTY_DEFAULT_STRING));
    }

    /**
     * Stores the passed purposes binary String in SharedPreferences
     *
     * @param context  Context used to access the SharedPreferences
     * @param purposes binary String to be stored
     */
    public static void setPurposesString(Context context, String purposes) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(PURPOSES, purposes).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Stores the passed GoogleAC String to the SharedPreferences
     * @param context   Context used to access the SharedPreferences
     * @param googleAC  The binary GoogleAC String with spec version and consented Ad technology Provider IDs
     */
    public static void setGoogleACString(Context context, String googleAC) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskWrites();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(GOOGLE_AC, googleAC).apply();
        CMPUtils.resetStrictMode(oldThreadPolicy);
    }

    /**
     * Stores the passed googleAC binary String in SharedPreferences
     *
     * @param context  Context used to access the SharedPreferences
     */
    public static String getGoogleACString(Context context) {
        StrictMode.ThreadPolicy oldThreadPolicy = CMPUtils.enableDiskReads();
        return CMPUtils.resetStrictMode(oldThreadPolicy, PreferenceManager.getDefaultSharedPreferences(context).getString(GOOGLE_AC, EMPTY_DEFAULT_STRING));
    }

    /**
     * Clears the Storage
     * @param context The Context of the App.
     */
    public static void clearConsents(Context context) {
        setUSPrivacyString(context, null);
        setVendorsString(context, null);
        setPurposesString(context, null);
        setGoogleACString(context, null);
        setConsentString(context, null);
    }

}