package net.consentmanager.tvsdk.core.exceptions;

public class CMPConsentToolNetworkException extends Throwable {
    public CMPConsentToolNetworkException(String message) {
        super(message);
    }
}
