package net.consentmanager.tvsdk.core.exceptions;

public class CMPConsentToolException extends RuntimeException{

    private CMPConsentToolException() {}

    public CMPConsentToolException(String message) {
        super(message);
    }
}
