package net.consentmanager.tvsdk.core.callbacks;

import net.consentmanager.tvsdk.activities.CMPConsentToolActivity;

/**
 * Provides a listener that will be called when {@link CMPConsentToolActivity} opens the WebView
 */
public interface OnOpenCallback {

    /**
     * Listener called when {@link CMPConsentToolActivity} opens the WebView
     */
    void onWebViewOpened();

}