package net.consentmanager.tvsdk.core.callbacks;

import net.consentmanager.tvsdk.model.CMPSettings;
import net.consentmanager.tvsdk.server.CmpStatus;

/**
 * Provides a listener that will be called when a custom WebView should be opened, apart from the default one.
 */
public interface CustomOpenActionCallback {

    /**
     * Listener called when a custom WebView should be opened, apart from the default one.
     */
    void onOpenCMPConsentToolActivity(CmpStatus response, CMPSettings settings);

}