package net.consentmanager.tvsdk.core.callbacks;

public interface OnParseConsentCallback {
    void parse(String cmpData);
}
