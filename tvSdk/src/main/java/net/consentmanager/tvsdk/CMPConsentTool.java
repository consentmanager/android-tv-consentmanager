package net.consentmanager.tvsdk;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;

import net.consentmanager.tvsdk.core.callbacks.OnCloseCallback;
import net.consentmanager.tvsdk.core.callbacks.OnErrorDialogCallback;
import net.consentmanager.tvsdk.core.callbacks.OnNetworkExceptionCallback;
import net.consentmanager.tvsdk.core.callbacks.OnOpenCallback;
import net.consentmanager.tvsdk.core.callbacks.OnParseConsentCallback;
import net.consentmanager.tvsdk.core.exceptions.CMPConsentToolInitialiseException;
import net.consentmanager.tvsdk.core.exceptions.CMPConsentToolNetworkException;
import net.consentmanager.tvsdk.core.exceptions.CMPConsentToolSettingsException;
import net.consentmanager.tvsdk.model.CMPConfig;
import net.consentmanager.tvsdk.model.CMPSettings;
import net.consentmanager.tvsdk.model.CMPUtils;
import net.consentmanager.tvsdk.model.PublisherRestriction;
import net.consentmanager.tvsdk.server.CmpApi;
import net.consentmanager.tvsdk.server.CmpStatus;
import net.consentmanager.tvsdk.storage.CMPPrivateStorage;
import net.consentmanager.tvsdk.storage.CMPStorageConsentManager;
import net.consentmanager.tvsdk.storage.CMPStorageV1;
import net.consentmanager.tvsdk.storage.CMPStorageV2;
import net.consentmanager.tvsdk.activities.CMPConsentToolActivity;
import net.consentmanager.tvsdk.core.callbacks.CustomOpenActionCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This is the Main class providing all functionalities of the CMP Consent Tool.
 * You have to initialise this class with the method createInstance(...). The config
 * data needs to be send via the method or been declared in the AndroidManifest of this module.
 * The following three information must be given:
 * <meta-data android:name="net.consentmanager.tvsdk.ID" android:value="<Your CMP ID>" />
 * <meta-data android:name="net.consentmanager.tvsdk.SERVER_DOMAIN" android:value="<The Server Domain that needs to be called>" />
 * <meta-data android:name="net.consentmanager.tvsdk.APP_NAME" android:value="<The Name of your app, as mentioned in your account>" />
 * <meta-data android:name="net.consentmanager.tvsdk.LANGUAGE" android:value="<The Language of the consent values>" />
 * These information are not required, if you instantiate this class with the config Parameters in your main class.
 *
 */
public class CMPConsentTool{

    private static final String TAG = "CMPConsentTool";
    /**
     * If log values should be presented
     */
    private static boolean logMode = false;
    /**
     * The singleton CMPConsentTool class. Only over this class you can attemp the functionalities
     */
    private static CMPConsentTool consentTool;
    /**
     * The config, which holds the correct Config parameters, explained at the class description.
     */
    private CMPConfig config;

    /**
     * A function that will be called, when the Modal View will be closed.
     */
    private OnCloseCallback closeListener;

    /**
     * A function that will be called, when the Modal View will be opened.
     */
    private OnOpenCallback openListener;

    /**
     * A function that will be called, if an error Message will be send from the server.
     */
    private OnErrorDialogCallback errorDialog;

    /**
     * If set, this function will be called, if the Server send a request.
     */
    private CustomOpenActionCallback customAction;
    /**
     * If set, this function will be called, if the Server send a request.
     */
    private OnNetworkExceptionCallback networkErrorHandler;

    /**
     * The last consentTool ServerResponse
     */
    private CmpStatus response;

    /**
     * Default value for the timeoutfunction while connecting to servers or loading web content
     */
    private static int TIMEOUT_DEF = 5000;

    /**
     * The timeout value for all Network requests
     */
    private int timeout;

    /**
     * Constructor can only be called through the static functions. This creates the Functions, witch
     * includes all functionalities of consentManager.
     * @param context The app Context
     * @param config  The Config for Consent Manager
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     */
    private CMPConsentTool(Context context, CMPConfig config, int timeout) {
        this(context, config,new OnOpenCallback() {
            @Override
            public void onWebViewOpened() {

            }
        },  new OnCloseCallback() {
            @Override
            public void onWebViewClosed() {

            }
        }, timeout);
    }

    /**
     * Constructor can only be called through the static functions. This creates the Functions, witch
     * includes all functionalities of consentManager.
     * @param context The app Context
     * @param config  The Config for Consent Manager
     */
    private CMPConsentTool(Context context, CMPConfig config) {
        this(context, config, TIMEOUT_DEF);
    }

    

    /**
     * Constructor can only be called through the static functions. This creates the Functions, witch
     * includes all functionalities of consentManager.
     * @param context The app Context
     * @param config  The Config for Consent Manager
     * @param openListener Listener which is called, if the WebView will be opened
     */
    private CMPConsentTool(Context context, CMPConfig config, OnOpenCallback openListener) {
        this(context, config, openListener,TIMEOUT_DEF);
    }

    /**
     * Constructor can only be called through the static functions. This creates the Functions, witch
     * includes all functionalities of consentManager.
     * @param context The app Context
     * @param config  The Config for Consent Manager
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or due to acception and the consent String was parsed
     */
    private CMPConsentTool(Context context, CMPConfig config, OnCloseCallback closeListener) {
        this(context, config, closeListener, TIMEOUT_DEF);
    }

    /**
     * Constructor can only be called through the static functions. This creates the Functions, witch
     * includes all functionalities of consentManager.
     * @param context The app Context
     * @param config  The Config for Consent Manager
     * @param openListener Listener which is called, if the WebView will be opened
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or due to acception and the consent String was parsed
     */
    private CMPConsentTool(Context context, CMPConfig config, OnOpenCallback openListener, OnCloseCallback closeListener) {
        this(context, config, openListener, closeListener, TIMEOUT_DEF);
    }

    /**
     * Constructor can only be called through the static functions. This creates the Functions, witch
     * includes all functionalities of consentManager.
     * @param context The app Context
     * @param config  The Config for Consent Manager
     * @param openListener Listener which is called, if the WebView will be opened
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     */
    private CMPConsentTool(Context context, CMPConfig config, OnOpenCallback openListener, int timeout) {
        this(context, config, openListener, new OnCloseCallback() {
            @Override
            public void onWebViewClosed() {

            }
        }, timeout);
    }

    /**
     * Constructor can only be called through the static functions. This creates the Functions, witch
     * includes all functionalities of consentManager.
     * @param context The app Context
     * @param config  The Config for Consent Manager
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or due to acception and the consent String was parsed
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     */
    private CMPConsentTool(Context context, CMPConfig config, OnCloseCallback closeListener, int timeout) {
        this(context, config, new OnOpenCallback() {
            @Override
            public void onWebViewOpened() {

            }
        }, closeListener, timeout);
    }

    /**
     * Constructor can only be called through the static functions. This creates the Functions, witch
     * includes all functionalities of consentManager.
     * @param context The app Context
     * @param config  The Config for Consent Manager
     * @param openListener Listener which is called, if the WebView will be opened
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or due to acception and the consent String was parsed
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     */
    private CMPConsentTool(Context context, CMPConfig config, OnOpenCallback openListener, OnCloseCallback closeListener, int timeout) {
        this.setOpenCmpConsentToolViewListener(openListener);
        this.setCloseCmpConsentToolViewListener(closeListener);
        this.config = config;
        this.timeout = timeout;
        this.checkAndProceedConsentUpdate(context);
    }


    /**
     *
     * @param context
     * @param config
     * @param openListener
     * @param closeListener
     * @param timeout
     * @param adId
     */
    private CMPConsentTool(Context context, CMPConfig config, OnOpenCallback openListener, OnCloseCallback closeListener, int timeout, String adId) {
        this.setOpenCmpConsentToolViewListener(openListener);
        this.setCloseCmpConsentToolViewListener(closeListener);
        this.config = config;
        this.timeout = timeout;
        this.checkAndProceedConsentUpdate(context, adId);
    }

    /**
     *
     * @param context
     * @param config
     * @param timeout
     * @param idfa
     */
    public CMPConsentTool(Context context, CMPConfig config, int timeout, String idfa) {
        this.setOpenCmpConsentToolViewListener(new OnOpenCallback() {
                                                   @Override
                                                   public void onWebViewOpened() {

                                                   }
                                               }
        );
        this.setCloseCmpConsentToolViewListener(new OnCloseCallback() {
            @Override
            public void onWebViewClosed() {

            }
        });
        this.config = config;
        this.timeout = timeout;
        this.checkAndProceedConsentUpdate(context, idfa);
    }

    /**
     *
     * @param context
     * @param config
     * @param openListener
     * @param closeListener
     * @param adId
     * @return
     */
    public static CMPConsentTool createWithIDFA(Context context, CMPConfig config, OnOpenCallback openListener, OnCloseCallback closeListener, String adId) {
        if (!consentToolExists()) {
            consentTool = new CMPConsentTool(context, config, openListener, closeListener, 5000, adId);
        } else {
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     *
     * @param context
     * @param id
     * @param serverDomain
     * @param appName
     * @param lang
     * @param idfa
     * @return
     */
    public static CMPConsentTool createWIthIDFA(Context context, int id, String serverDomain, String appName, String lang, String idfa) {
        if (!consentToolExists()) {
            consentTool = new CMPConsentTool(context, CMPConfig.createInstance(id, serverDomain, appName, lang), 5000, idfa);
        } else {
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters given
     * in the AndroidManifest.xml . You need to use this or an other createInstant Method to
     * initialise the CMPConsentTool before you use the functionality.
     * @param context       The Context of the App
     * @return The created singleton Consent Manager Instance
     * @throws CMPConsentToolSettingsException If there are missing configs in the AndroidManifest.xml
     */
    public static CMPConsentTool createInstance(Context context){
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(context));
        } else {
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters given
     * in the AndroidManifest.xml . You need to use this or an other createInstant Method to
     * initialise the CMPConsentTool before you use the functionality.
     * @param context       The Context of the App
     * @param openListener Listener which is called, if the WebView will be opened
     * @return The created singleton Consent Manager Instance
     * @throws CMPConsentToolSettingsException If there are missing configs in the AndroidManifest.xml
     */
    public static CMPConsentTool createInstance(Context context, OnOpenCallback openListener){
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(context), openListener);
        } else {
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters given
     * in the AndroidManifest.xml . You need to use this or an other createInstant Method to
     * initialise the CMPConsentTool before you use the functionality.
     * @param context       The Context of the App
     * @param closeListener Listener which is called, if the WebView has been closed due to errors 
     *                      or due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     * @throws CMPConsentToolSettingsException If there are missing configs in the AndroidManifest.xml
     */
    public static CMPConsentTool createInstance(Context context, OnCloseCallback closeListener){
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(context), closeListener);
        } else {
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters given
     * in the AndroidManifest.xml . You need to use this or an other createInstant Method to
     * initialise the CMPConsentTool before you use the functionality.
     * @param context       The Context of the App
     * @param openListener Listener which is called, if the WebView will be opened
     * @param closeListener Listener which is called, if the WebView has been closed due to errors 
     *                      or due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     * @throws CMPConsentToolSettingsException If there are missing configs in the AndroidManifest.xml
     */
    public static CMPConsentTool createInstance(Context context, OnOpenCallback openListener, OnCloseCallback closeListener){
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(context), openListener, closeListener);
        } else {
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters given
     * in the AndroidManifest.xml . You need to use this or an other createInstant Method to
     * initialise the CMPConsentTool before you use the functionality.
     * @param context       The Context of the App
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @return The created singleton Consent Manager Instance
     * @throws CMPConsentToolSettingsException If there are missing configs in the AndroidManifest.xml
     */
    public static CMPConsentTool createInstance(Context context, int timeout){
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(context), timeout);
        } else {
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters given
     * in the AndroidManifest.xml . You need to use this or an other createInstant Method to
     * initialise the CMPConsentTool before you use the functionality.
     * @param context       The Context of the App
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @param openListener Listener which is called, if the WebView will be opened
     * @return The created singleton Consent Manager Instance
     * @throws CMPConsentToolSettingsException If there are missing configs in the AndroidManifest.xml
     */
    public static CMPConsentTool createInstance(Context context, int timeout, OnOpenCallback openListener){
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(context), timeout, openListener);
        } else {
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters given
     * in the AndroidManifest.xml . You need to use this or an other createInstant Method to
     * initialise the CMPConsentTool before you use the functionality.
     * @param context       The Context of the App
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or
     *                      due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     * @throws CMPConsentToolSettingsException If there are missing configs in the AndroidManifest.xml
     */
    public static CMPConsentTool createInstance(Context context, int timeout, OnCloseCallback closeListener){
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(context), timeout, closeListener);
        } else {
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters given
     * in the AndroidManifest.xml . You need to use this or an other createInstant Method to
     * initialise the CMPConsentTool before you use the functionality.
     * @param context       The Context of the App
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @param openListener Listener which is called, if the WebView will be opened
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or
     *                      due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     * @throws CMPConsentToolSettingsException If there are missing configs in the AndroidManifest.xml
     */
    public static CMPConsentTool createInstance(Context context, int timeout, OnOpenCallback openListener, OnCloseCallback closeListener){
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(context), timeout, openListener, closeListener);
        } else {
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context       The Context of the App
     * @param id The Id you got from consentmanager online platform
     * @param server_domain The Domain of the Server that should be called.
     * @param app_name  The name of your App.
     * @param language  The language of the ConsentScreen
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, int id, String server_domain, String app_name, String language) {
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(id, server_domain, app_name, language));
        } else {
            consentTool.setConfig(CMPConfig.createInstance(id, server_domain, app_name, language));
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context       The Context of the App
     * @param id The Id you got from consentmanager online platform
     * @param server_domain The Domain of the Server that should be called.
     * @param app_name  The name of your App.
     * @param language  The language of the ConsentScreen
     * @param openListener Listener which is called, if the WebView will be opened
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, int id, String server_domain, String app_name, String language, OnOpenCallback openListener) {
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(id, server_domain, app_name, language), openListener);
        } else {
            consentTool.setConfig(CMPConfig.createInstance(id, server_domain, app_name, language));
            consentTool.setOpenCmpConsentToolViewListener(openListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context       The Context of the App
     * @param id The Id you got from consentmanager online platform
     * @param server_domain The Domain of the Server that should be called.
     * @param app_name  The name of your App.
     * @param language  The language of the ConsentScreen
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or
     *                      due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, int id, String server_domain, String app_name, String language, OnCloseCallback closeListener) {
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(id, server_domain, app_name, language), closeListener);
        } else {
            consentTool.setConfig(CMPConfig.createInstance(id, server_domain, app_name, language));
            consentTool.setCloseCmpConsentToolViewListener(closeListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context       The Context of the App
     * @param id The Id you got from consentmanager online platform
     * @param server_domain The Domain of the Server that should be called.
     * @param app_name  The name of your App.
     * @param language  The language of the ConsentScreen
     * @param openListener Listener which is called, if the WebView will be opened
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or
     *                      due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, int id, String server_domain, String app_name, String language, OnOpenCallback openListener, OnCloseCallback closeListener) {
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(id, server_domain, app_name, language), openListener, closeListener);
        } else {
            consentTool.setConfig(CMPConfig.createInstance(id, server_domain, app_name, language));
            consentTool.setCloseCmpConsentToolViewListener(closeListener);
            consentTool.setOpenCmpConsentToolViewListener(openListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context       The Context of the App
     * @param id The Id you got from consentmanager online platform
     * @param server_domain The Domain of the Server that should be called.
     * @param app_name  The name of your App.
     * @param language  The language of the ConsentScreen
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, int id, String server_domain, String app_name, String language, int timeout) {
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(id, server_domain, app_name, language), timeout);
        } else {
            consentTool.setConfig(CMPConfig.createInstance(id, server_domain, app_name, language));
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context       The Context of the App
     * @param id The Id you got from consentmanager online platform
     * @param server_domain The Domain of the Server that should be called.
     * @param app_name  The name of your App.
     * @param language  The language of the ConsentScreen
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @param openListener Listener which is called, if the WebView will be opened
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, int id, String server_domain, String app_name, String language, int timeout, OnOpenCallback openListener) {
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(id, server_domain, app_name, language), timeout, openListener);
        } else {
            consentTool.setConfig(CMPConfig.createInstance(id, server_domain, app_name, language));
            consentTool.setOpenCmpConsentToolViewListener(openListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context       The Context of the App
     * @param id The Id you got from consentmanager online platform
     * @param server_domain The Domain of the Server that should be called.
     * @param app_name  The name of your App.
     * @param language  The language of the ConsentScreen
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or
     *                      due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, int id, String server_domain, String app_name, String language, int timeout, OnCloseCallback closeListener) {
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(id, server_domain, app_name, language), timeout, closeListener);
        } else {
            consentTool.setConfig(CMPConfig.createInstance(id, server_domain, app_name, language));
            consentTool.setCloseCmpConsentToolViewListener(closeListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and the config parameters. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context       The Context of the App
     * @param id The Id you got from consentmanager online platform
     * @param server_domain The Domain of the Server that should be called.
     * @param app_name  The name of your App.
     * @param language  The language of the ConsentScreen
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @param openListener Listener which is called, if the WebView will be opened
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or
     *                      due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, int id, String server_domain, String app_name, String language, int timeout, OnOpenCallback openListener, OnCloseCallback closeListener) {
        if(!consentToolExists()) {
            consentTool = createInstance(context, CMPConfig.createInstance(id, server_domain, app_name, language), timeout, openListener, closeListener);
        } else {
            consentTool.setConfig(CMPConfig.createInstance(id, server_domain, app_name, language));
            consentTool.setCloseCmpConsentToolViewListener(closeListener);
            consentTool.setOpenCmpConsentToolViewListener(openListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and a given Config. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context  The Context of the App
     * @param config The filled CMP Config.
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, CMPConfig config){
        if(!consentToolExists()) {
            consentTool = new CMPConsentTool(context, config);
            log("Created an new CMPConsentTool Instance");
        } else {
            consentTool.setConfig(config);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and a given Config. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context  The Context of the App
     * @param config The filled CMP Config.
     * @param openListener Listener which is called, if the WebView will be opened
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, CMPConfig config, OnOpenCallback openListener ){
        if(!consentToolExists()) {
            consentTool = new CMPConsentTool(context, config, openListener);
            log("Created an new CMPConsentTool Instance");
        } else {
            consentTool.setConfig(config);
            consentTool.setOpenCmpConsentToolViewListener(openListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and a given Config. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context  The Context of the App
     * @param config The filled CMP Config.
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or
     *                      due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, CMPConfig config, OnCloseCallback closeListener ){
        if(!consentToolExists()) {
            consentTool = new CMPConsentTool(context, config, closeListener);
            log("Created an new CMPConsentTool Instance");
        } else {
            consentTool.setConfig(config);
            consentTool.setCloseCmpConsentToolViewListener(closeListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and a given Config. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context  The Context of the App
     * @param config The filled CMP Config.
     * @param openListener Listener which is called, if the WebView will be opened
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or
     *                      due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, CMPConfig config, OnOpenCallback openListener, OnCloseCallback closeListener ){
        if(!consentToolExists()) {
            consentTool = new CMPConsentTool(context, config, openListener, closeListener);
            log("Created an new CMPConsentTool Instance");
        } else {
            consentTool.setConfig(config);
            consentTool.setCloseCmpConsentToolViewListener(closeListener);
            consentTool.setOpenCmpConsentToolViewListener(openListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and a given Config. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context  The Context of the App
     * @param config The filled CMP Config.
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, CMPConfig config, int timeout){
        if(!consentToolExists()) {
            consentTool = new CMPConsentTool(context, config, timeout);
            log("Created an new CMPConsentTool Instance");
        } else {
            consentTool.setConfig(config);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and a given Config. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context  The Context of the App
     * @param config The filled CMP Config.
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @param openListener Listener which is called, if the WebView will be opened
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, CMPConfig config, int timeout, OnOpenCallback openListener ){
        if(!consentToolExists()) {
            consentTool = new CMPConsentTool(context, config, openListener, timeout);
            log("Created an new CMPConsentTool Instance");
        } else {
            consentTool.setConfig(config);
            consentTool.setOpenCmpConsentToolViewListener(openListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and a given Config. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context  The Context of the App
     * @param config The filled CMP Config.
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or
     *                      due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, CMPConfig config, int timeout, OnCloseCallback closeListener ){
        if(!consentToolExists()) {
            consentTool = new CMPConsentTool(context, config, closeListener, timeout);
            log("Created an new CMPConsentTool Instance");
        } else {
            consentTool.setConfig(config);
            consentTool.setCloseCmpConsentToolViewListener(closeListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Initialises the CMPConsentTool with the given context and a given Config. You need to
     * use this or an other createInstant Method to initialise the CMPConsentTool before you use the
     * functionality. Alternatively, you can insert these parameters in the AndroidManifest.xml to
     * make your code more readable and to have all your configs on one place.
     * @param context  The Context of the App
     * @param config The filled CMP Config.
     * @param timeout   After how many seconds (1000 = 1sek) the request to a server or to show the Webwview will be quited. Def = 5sek;
     * @param openListener Listener which is called, if the WebView will be opened
     * @param closeListener Listener which is called, if the WebView has been closed due to errors or
     *                      due to acception and the consent String was parsed
     * @return The created singleton Consent Manager Instance
     */
    public static CMPConsentTool createInstance(Context context, CMPConfig config, int timeout, OnOpenCallback openListener, OnCloseCallback closeListener ){
        if(!consentToolExists()) {
            consentTool = new CMPConsentTool(context, config, openListener, closeListener, timeout);
            log("Created an new CMPConsentTool Instance");
        } else {
            consentTool.setConfig(config);
            consentTool.setCloseCmpConsentToolViewListener(closeListener);
            consentTool.setOpenCmpConsentToolViewListener(openListener);
            log("Found an existing CMPConsentTool Instance. Please use getInstance instead!");
        }
        return consentTool;
    }

    /**
     * Sets the Config to the Consent Manager
     * @param config The ne config
     */
    private void setConfig(CMPConfig config) { this.config = config; }

    /**
     * Returns the Config that is set
     * @return the set Config
     */
    public CMPConfig getConfig() { return config; }

    /**
     * Returns the CMPConsentTool. If you have not initialised the CMPConsentTool before,
     * The CMPConsentToolInitialisation Exception will be thrown.
     * @return the initialised singleton Instant of the consent Manager.
     * @throws CMPConsentToolInitialiseException If the CMPConsentTool wasn't initialised
     */
    public static CMPConsentTool getInstance(){
        if ( consentTool == null){
            throw new CMPConsentToolInitialiseException();
        }
        return consentTool;
    }

    /**
     * Returns the CMPConsentTool, or null if it wasn't initialised. This method is unsafe,
     * because it can lead to null pointer exceptions, but if you are sure, the ConsentTool
     * was initialised before, you can use this Method, without the need to catch the error.
     * We recommend to save the returned Object from the createInstant Method, to use the
     * Methods of the consentManager.
     * @return the initialised singleton Instant of the consent Manager, or null.
     */
    public static CMPConsentTool getInstanceUnsafe(){
        return consentTool;
    }
    
    /**
     * If the ConsentManager was initialised
     * return If the ConsentManager was initialised
     */
    private static boolean consentToolExists(){
        return consentTool != null;
    }

    /**
     * Sets a Listener to the given button, If the Button is clicked, a modal view will be displayed
     * with the consent web view. If the Compliance is accepted or rejected, a close function will be
     * called. You can override this close function with your own. Therefor implement the OnCloseCallback
     * and add this as an other parameter. If the parameter is not set, but the setCloseCmpConsentToolViewListener
     * was used to add a listener to the close event, this will be used.
     * @param context       The Context of the App
     * @param gdprButton The Button, the openCmpConsentToolViewListener should be added to.
     */
    public void setOpenCmpConsentToolViewListener(Context context, Button gdprButton){
        this.setOpenCmpConsentToolViewListener(context, gdprButton, this.closeListener);
    }

    /**
     * Sets a Listener to the given button, If the Button is clicked, a modal view will be displayed
     * with the consent web view. If the Compliance is accepted or rejected, a close function will be
     * called. You can override this close function with your own. Therefor implement the OnCloseCallback
     * and add this as the last parameter.
     * @param context       The Context of the App
     * @param gdprButton The Button, the openCmpConsentToolViewListener should be added to.
     * @param callback The OnCloseCallback, that should be called, when the button was closed.
     */
    public void setOpenCmpConsentToolViewListener(Context context, Button gdprButton, final OnCloseCallback callback){
        gdprButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CMPConsentTool.this.openCmpConsentToolView(context, callback);
            }
        });
        log("OpenCmpConsentToolViewListener initialised");
    }

    /**
     * Sets a listener that is called, if a network error occurs.
     * @param callback The OnErrorNetworkCallback that needs to be called.
     */
    public void setNetworkExceptionListener(final OnNetworkExceptionCallback callback){
        this.networkErrorHandler = callback;
        log("NetworkExceptionListener initialised");
    }

    /**
     * Sets a Listener. If the Compliance is accepted or rejected, this function will be
     * called. You can override this close function with your own. Therefor implement the OnCloseCallback
     * and add this as the parameter.
     * @param callback The OnCloseCallback, that should be called, when the View was closed.
     */
    public void setCloseCmpConsentToolViewListener(final OnCloseCallback callback){
        this.closeListener = callback;
        log("CloseCmpConsentToolViewListener initialised");
    }

    /**
     * Sets a Listener. If the Compliance View is opened this function will be called.
     * You can override this open function with your own. Therefor implement the OnOpenCallback
     * and add this as the parameter.
     * @param callback The OnOpenCallback, that should be called, when the View will be opened.
     */
    public void setOpenCmpConsentToolViewListener(final OnOpenCallback callback){
        this.openListener = callback;
        log("OpenCmpConsentToolViewListener initialised");
    }

    /**
     * Sets a Listener. If the Compliance View is opened this function will be called.
     * You can override this open function with your own. Therefor implement the OnOpenCallback
     * and add this as the parameter.
     * @param callback The OnOpenCallback, that should be called, when the View will be opened.
     */
    public void setErrorDialogCmpConsentToolViewListener(final OnErrorDialogCallback callback){
        this.errorDialog = callback;
        log("ErrorDialogCmpConsentToolViewListener initialised");
    }

    /**
     * Sets a Custom action to a server response.
     * @param customAction The customAction that should be called, apart from showing the consentTool
     */
    public void setCustomCmpConsentToolViewAction(CustomOpenActionCallback customAction){
        this.customAction = customAction;
        log("CustomCmpConsentToolViewAction initialised");
    }

    /**
     * Displays a modal view with the consent web view. If the Compliance is accepted or rejected,
     * a close function will be called. You can override this close function with your own. Therefor
     * implement the OnCloseCallback and add this as a parameter.
     * @param context       The Context of the App
     */
    public void openCmpConsentToolView(Context context){
        openCmpConsentToolView(context, this.closeListener);
    }

    /**
     * Displays a modal view with the consent web view. If the Compliance is accepted or rejected,
     * a close function will be called. You can override this close function with your own. Therefor
     * implement the OnCloseCallback and give it to this function. This Method will not send a request
     * to the consentTool Server again. It will use the last state. If you only wnt to open the consent
     * Tool View again, if the server gives a response status == 1 use the checkAndProceedConsentUpdate
     * method.
     * @param context       The Context of the App
     * @param callback The OnCloseCallback, that should be called, when the button was closed.
     */
    public void openCmpConsentToolView(Context context, OnCloseCallback callback){
        this.openListener.onWebViewOpened();
        log("openListener called");
        CMPStorageV1.setCmpPresentValue(context, true);
        log("CmpPresentValue set to true (visible)");
        if( this.customAction != null){
            this.customAction.onOpenCMPConsentToolActivity(response, CMPSettings.asInstance(context));
            log("custom action called");
            return;
        }
        if( CMPSettings.getConsentToolUrl(context) == ""){
            try {
                CmpApi.getAndSaveResponse(config, context, timeout);
            } catch(CMPConsentToolNetworkException e){
                log("Network is not available to get consentToolUrl for the Webview");
                CMPStorageV1.setCmpPresentValue(context, false);
                if( callback != null){
                    callback.onWebViewClosed();
                }
                return;
            }
        }
        if( CMPSettings.getConsentToolUrl(context) != "") {
            CMPConsentToolActivity.openCmpConsentToolView(context, getLastConsentString(context), CMPSettings.getConsentToolUrl(context), new OnParseConsentCallback() {
                @Override
                public void parse(String cmpData) {
                    CMPStorageV1.setCmpPresentValue(context, false);
                    if (cmpData != null) {
                        CMPUtils.parseAndSaveCMPDataString(context, cmpData);
                    }
                    if (callback != null) {
                        callback.onWebViewClosed();
                    }
                }
            }, networkErrorHandler, timeout);
        } else {
            if(networkErrorHandler != null){
                networkErrorHandler.onErrorOccur("The Consent URL was not returned from the consent Server");
            }
        }
    }

    /**
     * Returns the last given Consent, presented in base64 by consentManager
     * @return the last set consentString
     */
    private String getLastConsentString(Context context) {
        return CMPStorageConsentManager.getConsentString(context);
    }

    /**
     * This Method will request the consent Tool server for updates and updates the CMPSettings. The
     * Server Connections Parameters are set through the CMPConfig while initialising this instance or
     * put them in the AndroidManifest.xml
     * If the response from the Server has the status 0, nothing is done,
     * If the response from the Server has the status 1, the View is displayed, or the customAction is
     * called, if given.
     * If the response from the Server has the status 2, the message from the Server is shown as a
     * display Alert, or the errorDialog Listener is called, if given.
     * @param context       The Context of the App
     */
    private void checkAndProceedConsentUpdate(Context context){
        if(needsServerUpdate(context)){
            log("A day past since last server request");
            response = proceedServerRequest(context);
            if( response != null) {
                log(String.format("consentmanager send following response( STATUS:%d, REGULATION:%d, MESSAGE:%s, URL:%s)", response.getStatus(), response.getRegulation(), response.getMessage(), response.getUrl()));
                switch (response.getStatus()) {
                    case 0:
                        return;
                    case 1:
                        CMPPrivateStorage.setNeedsAcceptance(context, true);
                        openCmpConsentToolView(context);
                        return;
                    default:
                        showErrorMessage(context, response.getMessage());
                }
            } else {
                log("No update Needed. ConsentManager is not available");
            }
        } else if(needsConsentAcceptance(context)){
            log("A day past since last acceptance");
            openCmpConsentToolView(context);
        } else {
            log("No update Needed. Consent was requested today and acceptance was also given");
        }
    }

    /**
     * This Method will request the consent Tool server for updates and updates the CMPSettings. The
     * Server Connections Parameters are set through the CMPConfig while initialising this instance or
     * put them in the AndroidManifest.xml
     * If the response from the Server has the status 0, nothing is done,
     * If the response from the Server has the status 1, the View is displayed, or the customAction is
     * called, if given.
     * If the response from the Server has the status 2, the message from the Server is shown as a
     * display Alert, or the errorDialog Listener is called, if given.
     * @param context       The Context of the App
     */
    private void checkAndProceedConsentUpdate(Context context, String idfa){
        if(needsServerUpdate(context)){
            log("A day past since last server request");
            response = proceedServerRequest(context, idfa);
            if( response != null) {
                log(String.format("consentmanager send following response( STATUS:%d, REGULATION:%d, MESSAGE:%s, URL:%s)", response.getStatus(), response.getRegulation(), response.getMessage(), response.getUrl()));
                switch (response.getStatus()) {
                    case 0:
                        return;
                    case 1:
                        CMPPrivateStorage.setNeedsAcceptance(context, true);
                        openCmpConsentToolView(context);
                        return;
                    default:
                        showErrorMessage(context, response.getMessage());
                }
            } else {
                log("No update Needed. ConsentManager is not available");
            }
        } else if(needsConsentAcceptance(context)){
            log("A day past since last acceptance");
            openCmpConsentToolView(context);
        } else {
            log("No update Needed. Consent was requested today and acceptance was also given");
        }
    }

    /**
     * Shows an error Message or calls the errorDialog Listener, if given. The message, that needs
     * to be displayed is the given parameter.
     * @param context       The Context of the App
     * @param message Error message that was send from the consentTool Server.
     */
    private void showErrorMessage(Context context, final String message) {
        log("An error occured: " + message);
        if( errorDialog == null){
            new AlertDialog.Builder(context)
                    .setTitle("Error")
                    .setMessage(message)
                    .setNegativeButton("Accept", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }})
                    .show();
        } else {
            errorDialog.onErrorOccur(message);
        }
    }

    /**
     * If the CMPSettings need to be updated from teh server, cause they were not at this day.
     * @param context       The Context of the App
     * @return If they need to be updated
     */
    private boolean needsServerUpdate(Context context){
        return ! calledThisDay(context);
    }

    /**
     * If the CMPSettings need to be updated from teh server, cause they were not at this day.
     * @param context       The Context of the App
     * @return If they need to be updated
     */
    private boolean needsConsentAcceptance(Context context){
        return needsAcceptance(context);
    }

    /**
     * Proceeds the request to the Server declared in the CPMConfig and returns the ServerResponse.
     * @param context       The Context of the App
     * @return The Response from the Server
     */
    private CmpStatus proceedServerRequest(Context context) {
        log("Proceeding a new server Request");
        try {
            return CmpApi.getAndSaveResponse(config, context, timeout);
        } catch (CMPConsentToolNetworkException e) {
            log("A Network Exception occured");
            if( networkErrorHandler != null){
                networkErrorHandler.onErrorOccur(e.getMessage());
            }
        }
        return null;
    }

    private CmpStatus proceedServerRequest(Context context, String idfa) {
        try {
            return CmpApi.getAndSaveResponse(config, context, timeout, idfa);
        }
        catch (CMPConsentToolNetworkException e) {
            log("A Network Exception occured");
            if (networkErrorHandler != null) {
                networkErrorHandler.onErrorOccur(e.getMessage());
            }
            return null;
        }
    }


    /**
     * Returns if the server was already contacted this day.
     * @param context       The Context of the App
     * @return if the server was already contacted this day.
     */
    public boolean calledThisDay(Context context){
        Date last = getCalledLast(context);
        if( last != null){
            Date now = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            return formatter.format(now).equals(formatter.format(last));
        }
        return false;
    }

    /**
     * Returns if the user needs to give a consent, cause he didn't do so in the past, or because
     * the consent Server returned, that a new consent is required
     * @param context       The Context of the App
     * @return if the user needs to give a consent
     */
    public boolean needsAcceptance(Context context){
        return CMPPrivateStorage.needsAcceptance(context);
    }

    /**
     * When the server was last contacted.
     * @param context       The Context of the App
     * @return the date, when the server was contacted for the last time
     */
    private Date getCalledLast(Context context){
        return CMPPrivateStorage.getLastRequested(context);
    }

    /**
     * Returns the Vendor String, that was set by consentmanager
     * @param context       The Context of the App
     * @return  The String of vendors that was set through consentmanager
     */
    public String getVendorsString(Context context){
        log("Searching for the current Vendors String");
        String value = CMPStorageConsentManager.getVendorsString(context);
        log("Found the following vendors String: ");
        log(value);
        if( value != null){
            return value;
        }
        return "";
    }

    /**
     * Returns the purposes String, that was set by consentmanager
     * @param context       The Context of the App
     * @return  The String of purposes that was set through consentmanager
     */
    public String getPurposes(Context context){
        log("Searching for the current Purposes String");
        String value = CMPStorageConsentManager.getPurposesString(context);
        log("Found the following purposes String: ");
        log(value);
        if( value != null){
            return value;
        }
        return "";
    }

    /**
     * Returns the US Privacy String, that was set by consentmanager
     * @param context       The Context of the App
     * @return  The US Privacy String of vendors that was set through consentmanager
     */
    public String getUSPrivacyString(Context context){
        log("Searching for the current USPrivacy String");
        String value = CMPStorageConsentManager.getUSPrivacyString(context);
        log("Found the following USPrivacy String: ");
        log(value);
        if( value != null){
            return value;
        }
        return "";
    }

    /**
     * Returns the US Privacy String, that was set by consentmanager
     * @param context       The Context of the App
     * @return  The US Privacy String of vendors that was set through consentmanager
     */
    public String getGoogleACString(Context context){
        log("Searching for the current GoogleAC String");
        String value = CMPStorageConsentManager.getGoogleACString(context);
        log("Found the following GoogleAC String: ");
        log(value);
        if( value != null){
            return value;
        }
        return "";
    }

    /**
    * Returns, if the Vendor (id) has the rights to set cookies
    * @param context       The Context of the App
    * @param id            The ID of the vendor
    * @param isIABVendor   If the vendor is set by IAB standard (V1 / V2)
    * @return  If the Vendor has the Consent to set cookies.
    */
   public boolean hasVendorConsent(Context context, String id, boolean isIABVendor){
       if( isIABVendor ){
           int idInt;
           try {
               idInt = Integer.parseInt(id);
           } catch(Exception e){
               return false;
           }
           String x = CMPStorageV2.getVendorConsents(context);
           if( x != "" && x != null && x.length() >= idInt && idInt > 0){
               if( x.charAt(idInt - 1) == '1'){
                   return true;
               } else {
                   return false;
               }
           }
           x = CMPStorageV1.getVendorsString(context);
           if( x != "" && x != null && x.length() >= idInt && idInt > 0){
               if( x.charAt(idInt - 1) == '1'){
                   return true;
               } else {
                   return false;
               }
           }
           return false;
       } else {
           String x = CMPStorageConsentManager.getVendorsString(context);
           if( x != null) {
               return x.contains(String.format("_%s_", id));
           }
           return false;
       }
   }

   /**
    * Returns, if the purpose (id) has the rights to set cookies
    * @param context       The Context of the App
    * @param id            The ID of the purpose
    * @param isIABPurpose  If the purpose is set by IAB standard (V1 / V2)
    * @return  If the purpose has the consent to set cookies.
    */
   public boolean hasPurposeConsent(Context context, String id, boolean isIABPurpose){
       if( isIABPurpose ){
           int idInt;
           try {
               idInt = Integer.parseInt(id);
           } catch(Exception e){
               return false;
           }
           String x = CMPStorageV2.getPurposeConsents(context);
           if( x != "" && x != null && x.length() >= idInt && idInt > 0){
               if( x.charAt(idInt - 1) == '1'){
                   return true;
               } else {
                   return false;
               }
           }

           x = CMPStorageV1.getPurposesString(context);
           if( x != "" && x != null && x.length() >= idInt && idInt > 0){
               if( x.charAt(idInt - 1) == '1'){
                   return true;
               } else {
                   return false;
               }
           }
           return false;
       } else {
           String x = CMPStorageConsentManager.getPurposesString(context);
           if( x != null) {
               return x.contains(String.format("_%s_", id));
           }
           return false;
       }
   }

    /**
     * Returns, if a Consent is given for a purpose for a vendor
     * @param context       The Context of the App
     * @param purposeId     The purpose Id
     * @param vendorId      The vendor Id
     * @return  If a consent is given or not
     */
    public boolean hasPurposeConsentForVendor(Context context, int purposeId, int vendorId){
        try{

            PublisherRestriction pr = CMPStorageV2.getPublisherRestriction(context, purposeId);
            return pr.hasVendor(vendorId);
        }catch(Exception e) {
            return false;
        }
    }

    /**
     * An import Method that can be called to import the full consentString generated by a ConsentWebView
     * into the Shared Preferences of this device.
     * @param context The app Context
     * @param cmpData The String that should be set. This need to be encoded base64
     * @return  If the Import was successful, or if errors occurs in the String.
     */
    public static boolean importCMPData(Context context, String cmpData){
        log(String.format("Importing CMP Data: %s", cmpData));
        try {
            CMPUtils.parseAndSaveCMPDataString(context, cmpData);
        }catch(Exception e){
            e.printStackTrace();
            System.err.println("The cmpData String has an error!");
            return false;
        }
        return true;
    }

    /**
     * An export Function that can be called to export the full consentString generated by a ConsentWebView
     * and saved in the Shared Preferences of this device.
     * @param context The app Context
     */
    public static String exportCMPData(Context context){
        log("Exporting cmpData");
        return CMPStorageConsentManager.getConsentString(context);
    }

    public static void setLogMode(boolean on){
        logMode = on;
        if( on){
            log("Log-Mode is enabled");
        } else {
            System.out.println("Log-Mode is disabled");
        }
    }

    public static void log(String message){
        if( logMode){
            System.out.println(message);
        }
    }

    public static void clearAllValues(Context context){
        CMPStorageV1.clearConsents(context);
        CMPStorageV2.clearConsents(context);
        CMPPrivateStorage.clearConsents(context);
        CMPStorageConsentManager.clearConsents(context);
    }

}
