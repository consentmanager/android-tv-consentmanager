package net.consentmanager.tvsdk.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import net.consentmanager.tvsdk.CMPConsentTool;
import net.consentmanager.tvsdk.core.callbacks.OnNetworkExceptionCallback;
import net.consentmanager.tvsdk.core.callbacks.OnParseConsentCallback;
import net.consentmanager.tvsdk.core.exceptions.CMPConsentToolNetworkException;
import net.consentmanager.tvsdk.factorys.ModalLayout;
import net.consentmanager.tvsdk.model.CMPConfig;
import net.consentmanager.tvsdk.model.CMPSettings;
import net.consentmanager.tvsdk.model.CMPUtils;
import net.consentmanager.tvsdk.factorys.WebViewCreator;
import net.consentmanager.tvsdk.server.CmpApi;
import net.consentmanager.tvsdk.server.CmpStatus;
import net.consentmanager.tvsdk.storage.CMPStorageConsentManager;
import net.consentmanager.tvsdk.storage.CMPStorageV1;
import net.consentmanager.tvsdk.storage.CMPStorageV2;

public class CMPConsentToolActivity extends AppCompatActivity {

    private static OnParseConsentCallback onCloseCallback;
    private static OnNetworkExceptionCallback onNetworkCallback;
    private static int timeoutVal;

    private static String consentToolUrl;
    private static String consentString;

    /**
     * Used to start the Activity where the consentToolUrl is loaded into WebView.
     * @param context     context
     * @param closeCallback listener called when CMPConsentToolActivity is closed after interacting with WebView
     */
    public static void openCmpConsentToolView(Context context, String consent, String consentUrl, OnParseConsentCallback closeCallback, OnNetworkExceptionCallback networkCallback, int timeout) {
        if( CMPUtils.hasInternetConnection(context)) {
            Intent cmpConsentToolIntent = new Intent(context, CMPConsentToolActivity.class);
            timeoutVal = timeout;
            consentToolUrl = consentUrl;
            consentString = consent;
            onCloseCallback = closeCallback;
            onNetworkCallback = networkCallback;

            cmpConsentToolIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(cmpConsentToolIntent);
       } else {
            CMPConsentTool.log("The Network is not reachable to show the WebView");
            if( networkCallback != null){
                networkCallback.onErrorOccur("The Network is not reachable to show the WebView");
            }
            closeCallback.parse(null);
        }
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CMPSettings cmpSettings = CMPSettings.asInstance(CMPConsentToolActivity.this);
        CMPConfig cmpConfig = CMPConfig.getInstance(CMPConsentToolActivity.this);

        if (TextUtils.isEmpty(cmpSettings.getConsentToolUrl())) {
            CMPStorageV1.clearConsents(CMPConsentToolActivity.this);
            CMPStorageV2.clearConsents(CMPConsentToolActivity.this);
            CMPStorageConsentManager.clearConsents(CMPConsentToolActivity.this);
            try {
                CmpStatus response = CmpApi.getAndSaveResponse(cmpConfig, this, timeoutVal);
                if (response.getUrl() == null && response.getUrl() == "") {
                    CMPStorageV1.clearConsents(CMPConsentToolActivity.this);
                    CMPStorageV2.clearConsents(CMPConsentToolActivity.this);
                    CMPStorageConsentManager.clearConsents(CMPConsentToolActivity.this);
                    CMPStorageV1.setCmpPresentValue(CMPConsentToolActivity.this, false);
                    finish();
                    return;
                }
            } catch (CMPConsentToolNetworkException errorNetworkException){
                CMPConsentTool.log("finished loading WebView");
                if( onNetworkCallback != null){
                    onNetworkCallback.onErrorOccur(errorNetworkException.getMessage());
                }
                CMPStorageV1.setCmpPresentValue(CMPConsentToolActivity.this, false);
                finish();
            }
        }

        createLayout();

        setupWebViewClient();
    }


    /**
     * Creates a WebView and puts it to a new Layer, without displaying it.
     */
    private void createLayout() {
        // create the Layout with the WebView and add it to the ContentView
        LinearLayout linearLayout = ModalLayout.initialise(this).getLayout(onNetworkCallback, timeoutVal);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        String url = getCleanedConsentToolUrl();
        CMPConsentTool.log("Showing WebView with url: " + url);

        WebViewCreator.initialise(this).getWebView(onNetworkCallback, timeoutVal).loadUrl(url);


        //remove Parent link, if set.
        if( (linearLayout != null && linearLayout.getParent() != null)){
            try {
                ((ViewGroup) linearLayout.getParent()).removeView(linearLayout);
            }catch(Exception ignore){}
        }

        //Set Layer to Screen.
        setContentView(linearLayout, layoutParams);
    }

    /**
     * Method to setup url for the WebView in order to show the last selected values.
     */
    private String getCleanedConsentToolUrl() {
        String url = consentToolUrl;

        if (consentString != null) {
            if(url != null && !url.contains("consent=")){
                url = url + "consent=" + consentString;
            } else if(url != null && url.contains("consent=&")) {
                url = url.replace("consent=", "consent=" + consentString);
            } else if(url != null && url.contains("consent=")) {
                url = url.replaceAll("consent=.*&", "consent=" + consentString + "&");
            }
        }
        return url;
    }

    private void setupWebViewClient() {
        GDPRWebViewClient gdprWebViewClient = new GDPRWebViewClient();
        WebViewCreator.initialise(this).getWebView(onNetworkCallback, timeoutVal).setWebViewClient(gdprWebViewClient);
    }

    @Override
    public void onBackPressed() {
        CMPConsentTool.log("Back Button pressed in WebView");
        if( WebViewCreator.initialise(this).getWebView(onNetworkCallback, timeoutVal).canGoBack()) {
            WebViewCreator.initialise(this).getWebView(onNetworkCallback, timeoutVal).goBack();
        }
    }

    private class GDPRWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return handleWebViewInteraction(view, url);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return handleWebViewInteraction(view, String.valueOf(request.getUrl()));
        }

        private boolean handleWebViewInteraction(WebView view, String url) {
            if( url != null && url.contains("consent://")) {
                CMPConsentTool.log("consent returned: " + url);
                onCloseCallback.parse(url);
                finish();
                return true;
            } else if( url != "" && url != null && !url.equals(getCleanedConsentToolUrl()) ){
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
                return true;
            }
            return false;
        }

    }




}