package net.consentmanager.tvsdk.factorys;

import android.content.Context;
import android.widget.LinearLayout;

import net.consentmanager.tvsdk.core.callbacks.OnNetworkExceptionCallback;

public class ModalLayout {
    private LinearLayout layout;
    private Context context;
    private static ModalLayout instance;

    private ModalLayout(Context context){
        this.context = context;
    }

    public static ModalLayout initialise(Context context){
        if( instance == null){
            instance = new ModalLayout(context);
        }
        return instance;
    }

    public LinearLayout getLayout(OnNetworkExceptionCallback timeoutfunction){
        if( layout == null){
            layout = this.createLayout(timeoutfunction);
        }
        return this.layout;
    }

    public LinearLayout getLayout(OnNetworkExceptionCallback timeoutfunction, int timeout){
        if( layout == null){
            layout = this.createLayout(timeoutfunction, timeout);
        }
        return this.layout;
    }

    public void removeLayout(){
        this.layout = null;
    }

    private LinearLayout createLayout(OnNetworkExceptionCallback timeoutfunction){

        // create the Layout
        LinearLayout linearLayout = new LinearLayout(context);

        //set the WebView to the Layout
        linearLayout.addView(WebViewCreator.initialise(context).getWebView(timeoutfunction));

        //Hide Layer
        linearLayout.setVisibility(LinearLayout.VISIBLE);

        return linearLayout;
    }

    private LinearLayout createLayout(OnNetworkExceptionCallback timeoutfunction, int timeout){

        // create the Layout
        LinearLayout linearLayout = new LinearLayout(context);

        //set the WebView to the Layout
        linearLayout.addView(WebViewCreator.initialise(context).getWebView(timeoutfunction, timeout));

        //Hide Layer
        linearLayout.setVisibility(LinearLayout.VISIBLE);

        return linearLayout;
    }
}
