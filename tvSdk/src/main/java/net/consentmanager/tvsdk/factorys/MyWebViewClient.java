package net.consentmanager.tvsdk.factorys;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.consentmanager.tvsdk.core.callbacks.OnNetworkExceptionCallback;

public class MyWebViewClient extends WebViewClient {
        private boolean timeout;
        private int timeoutVal;
        private final static int DEFAULT_TIMEOUT = 5000;
        private OnNetworkExceptionCallback onNetworkExceptionCallback;

        public MyWebViewClient(OnNetworkExceptionCallback onNetworkExceptionCallback, int timeoutVal) {
            this.timeout = true;
            this.timeoutVal = timeoutVal;
            this.onNetworkExceptionCallback = onNetworkExceptionCallback;
        }

        public MyWebViewClient(OnNetworkExceptionCallback onNetworkExceptionCallback) {
            this(onNetworkExceptionCallback, DEFAULT_TIMEOUT);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            new Thread(() -> {
                timeout = true;
                try {
                    Thread.sleep(timeoutVal);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(timeout) {
                    onNetworkExceptionCallback.onErrorOccur("WebView couldn't be loaded");
                }
            }).start();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            timeout = false;
        }
    }
