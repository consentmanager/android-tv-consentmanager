package net.consentmanager.tvsdk;

import android.util.Log;

import net.consentmanager.tvsdk.model.CMPUtils;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import static org.junit.Assert.assertEquals;

public class CMPUtilsUnitTest {

    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void decodeBase64Url_isCorrect() {
        String urlEncoded ="p>&uuml;&egrave;</p>%2B%2F-_ %2B=";
        String result;
        try {
         result = URLDecoder.decode(urlEncoded, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            result = "";
        }
        String compare = CMPUtils.cleanBase64ForBinary(urlEncoded);
        System.out.println(compare);
        System.out.println(result);
        assertEquals(result, compare);
    }


}
