# Consentmanager #
The ConsentManager TV Library for Android TV apps implements and provides functionality to inform the user about data protection and ask and collect consent from the user. It enables app developers to integrate the ConsentManager service into their app without difficulty. 

## Supported Standards ##

The ConsentManager GDPR/CCPA Library for iOS supports the following industry standards:

*IAB TCF v1
*IAB TCF v2
*IAB USPrivacy v1
*ConsentManager custom vendors/purposes
*Google Additional Consent Mode (Google AC String)

## Overview ##

* Installation
  - Gradles
  - maven
* Using the Library

## Installation ##

### Gradle ###
Step 1. Add the jitpack repository to your root build.gradle at the end of repositories:

```
	allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```

Step 2. Add the dependency to your apps build.gradle

```
	dependencies {
	        implementation 'org.bitbucket.consentmanager:android-tv-consentmanager:1.0.0'
	}
```


### maven ###


Step 1. Add the jitpack repository to your build.gradle at the end of repositories:

```
	<repositories>
		<repository>
		    <id>jitpack.io</id>
		    <url>https://jitpack.io</url>
		</repository>
	</repositories>
```

Step 2. Add the dependency to your apps build.gradle

```
	<dependency>
	    <groupId>org.bitbucket.consentmanager</groupId>
	    <artifactId>android-tv-consentmanager</artifactId>
	    <version>1.0.0</version>
	</dependency>
```

## Using the library ##


### Initiate ConsentTool ###

With the app-start (usually your viewDidAppear function) you must create an instance of class CMPConsentTool. This will automatically fetch the necessary data from our server and determine if the consent screen needs to be shown or not. If so, the SDK will automatically show the consent screen at this point, collect the data and provide the data to the app. The instance can then be used in order to get consent details from the SDK in order to use it in the app.

To initiate the ConsentTool, go to your targeted class and create a instance of CMPConsentTool like shown below: 

```
//...
import net.consentmanager.tvsdk.CMPConsentTool;
//...
public class MainActivity extends AppCompatActivity {
    private CMPConsentTool consentTool;
    //...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
      //..
      consentTool = CMPConsentTool.createInstance(this, 123456, "consentmanager.mgr.consensu.org", "MyFavouriteApp", "");
	//.. You can also instantiate the consentmanager with the advertisement id:
	// consentTool = CMPConsentTool.createWIthIDFA(this,123456,"www.consentmanager.mgr.consensu.org", "myFavouriteApp", "EN", "38400000-8cf0-11bd-b23e-10b96e40000d"); 

    }
//...
}
```

### Using the SDK ###

In order to check whether a vendor or purpose have consent, you can use the two methods:

```
if(consentTool.hasPurposeConsent(this,"52",false))
{
    if(consentTool.hasVendorConsent(this,"s26", false))
    {
        //do something with data
    }
}

```

[![](https://jitpack.io/v/org.bitbucket.consentmanager/android-tv-consentmanager.svg)](https://jitpack.io/#org.bitbucket.consentmanager/android-tv-consentmanager)